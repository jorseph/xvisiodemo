package com.compal.xvisiodemo;

import org.xvisio.xvsdk.TofListener;
import org.xvisio.xvsdk.XCamera;
import org.xvisio.xvsdk.DeviceListener;
import org.xvisio.xvsdk.ImuListener;
import org.xvisio.xvsdk.StereoListener;
import org.xvisio.xvsdk.PoseListener;
import org.xvisio.xvsdk.RgbListener;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.usens.HandInfo;
import com.xvisio.hand.Gesture;
import com.xvisio.hand.XvHandInfo;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "XVSDK Demo";

    private static final int PERMISSIONS_REQUEST_CAMERA = 0;
    private boolean mPermissionsGranted = false;


    private Context mAppContext = null;
    private XCamera mCamera = null;

    private boolean isMixedMode = true;

    Gesture ges = null;
    Handler gesHandler = new Handler();
    boolean gesInited = false;

    ImageView iv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mAppContext = getApplicationContext();

        setContentView(R.layout.activity_main);

        iv = findViewById(R.id.imageView);

        if (android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.O &&
                checkSelfPermission( Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions( new String[]{Manifest.permission.CAMERA}, PERMISSIONS_REQUEST_CAMERA);
            return;
        }

        mPermissionsGranted = true;

        ges = new Gesture(mAppContext);
        ges.setRgbStepMs(50);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (checkSelfPermission( Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions( new String[]{Manifest.permission.CAMERA}, PERMISSIONS_REQUEST_CAMERA);
            return;
        }
        mPermissionsGranted = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(mPermissionsGranted) {
            init();
        }
        else
            Log.e(TAG, "missing permissions");
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(mCamera != null) {
            mCamera.stopStreams();
        }
    }

    private void init(){
        if( mCamera == null ) {
            mCamera = new XCamera();
            mCamera.setDevicesChangedCallback(mListener);
            mCamera.setStereoCallback(mStereoListener);
            mCamera.setRgbCallback(mRgbListener);
            mCamera.setTofCallback(mTofListener);
            mCamera.init(mAppContext);
        }
        mCamera.startStreams();
    }

    Runnable gesGetter = new Runnable(){
        @Override
        public void run() {
            // TODO to optimize logic
            int gesid = -1;
            ArrayList<Integer> evs = new ArrayList<>();
            XvHandInfo[] xvInfos = ges.getXvHandInfos();
            if (xvInfos != null) {
                for (XvHandInfo xvInfo : xvInfos) {
                    for (HandInfo info : xvInfo.handInfos) {
                        gesid = info.gesture_;
                        if (info.event_ == 10 || info.event_ == 20) {
                            gesid = info.event_ + 50;
                        } else if (info.event_ == -1 || info.event_ == 21) {
                            // ignore
                        } else {
                            evs.add(info.event_);
                        }
                    }
                }
            }

            int drawid = R.drawable.logo;
            if (!evs.isEmpty()) {
                int lastEvId = evs.get(evs.size() - 1);
                drawid = getResources().getIdentifier(String.format("e%02d", lastEvId), "drawable", getPackageName());
            } else if (gesid != -1) {
                drawid = getResources().getIdentifier(String.format("g%02d", gesid), "drawable", getPackageName());
            }
            Bitmap bmp = BitmapFactory.decodeResource(getResources(), drawid);
            iv.setImageBitmap(bmp);

            gesHandler.postDelayed(this, 100);
        }
    };

    private DeviceListener mListener = new DeviceListener() {
        @Override
        public void onDeviceAttach() {
            Log.e(TAG, "onDeviceAttach ...");

            if (gesInited) {
                Log.e(TAG, "start ges");
                ges.start();
                gesHandler.postDelayed(gesGetter, 1000);
            }
        }

        @Override
        public void onDeviceAttach(int fd) {
            // must init ges in this function due to use hid
            gesInited = ges.init(fd);
            Log.e(TAG, "init xvisio gesture " + (gesInited ? "success" : "failed"));
        }

        @Override
        public void onDeviceDetach() {
        }
    };


    private StereoListener mStereoListener = new StereoListener() {
        @Override
        public void onStereo( int width, int height, int pixels[] ) {

            final int fwidth = width;
            final int fheight = height;
            final int[] fpixels = pixels;

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                }
            });
        }
    };

    private RgbListener mRgbListener = new RgbListener() {
        @Override
        public void onRgb( int width, int height, int pixels[] ) {

            final int fwidth = width;
            final int fheight = height;
            final int[] fpixels = pixels;

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ImageView s = findViewById(R.id.rgbView);
                    Bitmap bitmap = Bitmap.createBitmap(fwidth, fheight, Bitmap.Config.ARGB_8888);
                    bitmap.setPixels(fpixels, 0, fwidth, 0, 0, fwidth, fheight);
                    Bitmap scaled = Bitmap.createScaledBitmap(bitmap, fwidth/4, fheight/4, false);
                    bitmap.recycle();
                    s.setImageBitmap(scaled);
                }
            });
        }
    };


    private TofListener mTofListener = new TofListener() {
        @Override
        public void onTof( int width, int height, int pixels[] ) {

            final int fwidth = width;
            final int fheight = height;
            final int[] fpixels = pixels;

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ImageView s = findViewById(R.id.tofView);
                    Bitmap bitmap = Bitmap.createBitmap(fwidth, fheight, Bitmap.Config.ARGB_8888);
                    bitmap.setPixels(fpixels, 0, fwidth, 0, 0, fwidth, fheight);
                    s.setImageBitmap(bitmap);
                }
            });
        }
    };
}
