/**
 * Automatically generated file. DO NOT MODIFY
 */
package org.xvisio.xvsdk;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String LIBRARY_PACKAGE_NAME = "org.xvisio.xvsdk";
  public static final String BUILD_TYPE = "debug";
}
