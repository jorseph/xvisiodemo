package org.xvisio.xvsdk;

public interface DeviceListener {
    void onDeviceAttach();
    void onDeviceAttach(int fd); // before open fd
    void onDeviceDetach();
}
