package org.xvisio.xvsdk;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.util.Log;

public class XCamera extends XVisioClass{
    private static final String TAG = XCamera.class.getSimpleName();

    private static DeviceWatcher mDeviceWatcher;
    private DeviceListener mListener;

    private static final int PERMISSIONS_REQUEST_CAMERA = 0;

    public XCamera() {
        Log.d(TAG, "Creation");
        if(mDeviceWatcher == null)
            mDeviceWatcher = new DeviceWatcher();
    }

    private Context mContext;

    public void init(Context context) {
        mContext = context;

        if(mDeviceWatcher != null)
            mDeviceWatcher.init(context);

        initCallbacks();
    }

    public void pause() {

    }

    static public void requestPermissions(Activity activity, Context context) {
        if (android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.O &&
                context.checkSelfPermission( Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            activity.requestPermissions( new String[]{Manifest.permission.CAMERA}, PERMISSIONS_REQUEST_CAMERA);
            return;
        }
    }

    public void startStreams(){

    }
    public void stopStreams(){

    }

    @Override
    public void close() {
        removeDevicesChangedCallback();
    }

    public synchronized void setDevicesChangedCallback(DeviceListener listener) {
        removeDevicesChangedCallback();
        mListener = listener;
        if(mDeviceWatcher != null)
            mDeviceWatcher.addListener(mListener);
    }

    public synchronized void removeDevicesChangedCallback() {
        if(mListener != null && mDeviceWatcher != null)
            mDeviceWatcher.removeListener(mListener);
    }

    public synchronized void setSlamMode(int mode) {
        if(mListener != null && mDeviceWatcher != null)
            mDeviceWatcher.setSlamMode(mode);
    }

    static PoseListener mPoseListener;
    public synchronized void setPoseCallback(PoseListener listener) {
        mPoseListener = listener;
    }

    static ImuListener mImuListener;
    public synchronized void setImuCallback(ImuListener listener) {
        mImuListener = listener;
    }

    static StereoListener mStereoListener;
    public synchronized void setStereoCallback(StereoListener listener) {
        mStereoListener = listener;
    }
    static RgbListener mRgbListener;
    public synchronized void setRgbCallback(RgbListener listener) {
        mRgbListener = listener;
    }

    static TofListener mTofListener;
    public synchronized void setTofCallback(TofListener listener) {
        mTofListener = listener;
    }
    private static native void initCallbacks();


    public static void poseCallback( double x, double y, double z, double roll, double pitch, double yaw){
        if( mPoseListener != null ){
            mPoseListener.onPose( x, y, z, roll, pitch, yaw );
        }
    }

    public static void imuCallback( double x, double y, double z){
        if( mImuListener != null ){
            mImuListener.onImu( x, y, z );
        }
    }


    public static void stereoCallback( int width, int height, int data[] ){
        if( mStereoListener != null ){
            mStereoListener.onStereo( width, height, data );
        }
    }

    public static void rgbCallback( int width, int height, int data[] ){
        if( mRgbListener != null ){
            mRgbListener.onRgb( width, height, data );
        }
    }

    public static void tofCallback( int width, int height, int data[] ){
        if( mTofListener != null ){
            mTofListener.onTof( width, height, data );
        }
    }


}
