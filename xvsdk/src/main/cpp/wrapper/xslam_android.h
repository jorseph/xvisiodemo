//
// Created by nemot on 2021/4/26.
//

#ifndef XSLAM_DEMO_XSLAM_ANDROID_H
#define XSLAM_DEMO_XSLAM_ANDROID_H
#define EXPORT_API __attribute__ ((visibility ("default")))
/**
 * @brief Rotation and translation structure
 */
struct transform
{
    double rotation[9]; //!< Rotation matrix (row major)
    double translation[3]; //!< Translation vector
};

/**
 * @brief Polynomial Distortion Model
 */
struct pdm
{
    double K[11];
/**
    Projection and raytrace formula can be found here:
    https://docs.opencv.org/3.4.0/d4/d94/tutorial_camera_calibration.html

    K[0] : fx
    K[1] : fy
    K[2] : u0
    K[3] : v0
    K[4] : k1
    K[5] : k2
    K[6] : p1
    K[7] : p2
    K[8] : k3
    K[9] : image width
    K[10] : image height
*/
};

struct pdm_calibration
{
    transform extrinsic;
    pdm intrinsic;
};

struct stereo_pdm_calibration
{
    pdm_calibration calibrations[2];
};

EXPORT_API bool get_pose_prediction(double * poseData,double predictionTime);
EXPORT_API bool readStereoDisplayCalibration(stereo_pdm_calibration *calib);
#endif //XSLAM_DEMO_XSLAM_ANDROID_H
