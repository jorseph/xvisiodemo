#include <jni.h>
#include <memory>
#include <vector>

#include <string>
#include <regex>
#include <sstream>
#include <iostream>
#include <mutex>
#include <opencv2/opencv.hpp>
#include <android/log.h>

#include <chrono>

#include <xv-sdk.h>
#include <unistd.h>
#include "xslam_android.h"

#define LOG_TAG "xslam wrapper"
#define LOG_DEBUG(...)   do { __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__ ); } while(false)

static std::shared_ptr<xv::Device> device;

class androidout : public std::streambuf {
public:
    enum { bufsize = 128 }; // ... or some other suitable buffer size
    androidout() { this->setp(buffer, buffer + bufsize - 1); }

private:
    int overflow(int c)
    {
        if (c == traits_type::eof()) {
            *this->pptr() = traits_type::to_char_type(c);
            this->sbumpc();
        }
        return this->sync()? traits_type::eof(): traits_type::not_eof(c);
    }

    int sync()
    {
        int rc = 0;
        if (this->pbase() != this->pptr()) {
            char writebuf[bufsize+1];
            memcpy(writebuf, this->pbase(), this->pptr() - this->pbase());
            writebuf[this->pptr() - this->pbase()] = '\0';

            rc = __android_log_write(ANDROID_LOG_INFO, "std", writebuf) > 0;
            this->setp(buffer, buffer + bufsize - 1);
        }
        return rc;
    }

    char buffer[bufsize];
};

class androiderr : public std::streambuf {
public:
    enum { bufsize = 128 }; // ... or some other suitable buffer size
    androiderr() { this->setp(buffer, buffer + bufsize - 1); }

private:
    int overflow(int c)
    {
        if (c == traits_type::eof()) {
            *this->pptr() = traits_type::to_char_type(c);
            this->sbumpc();
        }
        return this->sync()? traits_type::eof(): traits_type::not_eof(c);
    }

    int sync()
    {
        int rc = 0;
        if (this->pbase() != this->pptr()) {
            char writebuf[bufsize+1];
            memcpy(writebuf, this->pbase(), this->pptr() - this->pbase());
            writebuf[this->pptr() - this->pbase()] = '\0';

            rc = __android_log_write(ANDROID_LOG_ERROR, "std", writebuf) > 0;
            this->setp(buffer, buffer + bufsize - 1);
        }
        return rc;
    }

    char buffer[bufsize];
};

void yuv2rgb(unsigned char*yuyv_image, int* rgb_image, int width, int height );

static bool m_ready = false;

static JavaVM* jvm = 0;
static jclass s_XCameraClass = nullptr;

static JNIEnv* s_imuEnv = nullptr;
static jmethodID s_imuCallback = nullptr;

static JNIEnv* s_tofEnv = nullptr;
static jmethodID s_tofCallback = nullptr;

static JNIEnv* s_stereoEnv = nullptr;
static jmethodID s_stereoCallback = nullptr;

static JNIEnv* s_rgbEnv = nullptr;
static jmethodID s_rgbCallback = nullptr;

static JNIEnv* s_poseEnv = nullptr;
static JNIEnv* s_poseEnv2 = nullptr;
static jmethodID s_poseCallback= nullptr;


bool get_pose_prediction(double *poseData,double predictionTime)
{
    //XSlam::pose_ptr pose;
    if(m_ready == false)
        return false;

    xv::Pose pose;

    if(device->slam()->getPose(pose,predictionTime) == false) {
        LOG_DEBUG("...%s...,getpose failed",__FUNCTION__ );
        return false;
    }

    auto  r = pose.rotation();
    auto  _quat = xv::rotationToQuaternion(r);
    poseData[0] = _quat[0];
    poseData[1] = _quat[1];
    poseData[2] = _quat[2];
    poseData[3] = _quat[3];

    poseData[4] = pose.x();
    poseData[5] = pose.y();
    poseData[6] = pose.z();
    return true;
}

bool readStereoDisplayCalibration(stereo_pdm_calibration *calib)
{
    LOG_DEBUG("...%s...",__FUNCTION__ );
    if(m_ready){
        auto display = device->display();
        if (display && display->calibration().size()==2) {
            LOG_DEBUG("...%s...,read display cablibration sucess.",__FUNCTION__ );
            //2 eyes
            for(int i=0;i<2;i++) {
                calib->calibrations[i].intrinsic.K[0] = display->calibration()[i].pdcm[0].fx;
                calib->calibrations[i].intrinsic.K[1] = display->calibration()[i].pdcm[0].fy;
                calib->calibrations[i].intrinsic.K[2] = display->calibration()[i].pdcm[0].u0;
                calib->calibrations[i].intrinsic.K[3] = display->calibration()[i].pdcm[0].v0;
                calib->calibrations[i].intrinsic.K[4] = display->calibration()[i].pdcm[0].distor[0];
                calib->calibrations[i].intrinsic.K[5] = display->calibration()[i].pdcm[0].distor[1];
                calib->calibrations[i].intrinsic.K[6] = display->calibration()[i].pdcm[0].distor[2];
                calib->calibrations[i].intrinsic.K[7] = display->calibration()[i].pdcm[0].distor[3];
                calib->calibrations[i].intrinsic.K[8] = display->calibration()[i].pdcm[0].distor[4];
                calib->calibrations[i].intrinsic.K[9] = display->calibration()[i].pdcm[0].w;
                calib->calibrations[i].intrinsic.K[10] = display->calibration()[i].pdcm[0].h;

                std::memcpy(calib->calibrations[i].extrinsic.rotation, &((xv::Matrix3d)display->calibration()[i].pose.rotation())[0], sizeof(xv::Matrix3d));
                std::memcpy(calib->calibrations[i].extrinsic.translation, &((xv::Vector3d)display->calibration()[i].pose.translation())[0], sizeof(xv::Vector3d));
                LOG_DEBUG("display calibration,rotation size:%u,translation size:%u{",
                                                    (unsigned  int)sizeof(xv::Matrix3d),(unsigned  int)sizeof(xv::Vector3d));
                LOG_DEBUG("T%d:%lf,%lf,%lf",i+1,
                             calib->calibrations[i].extrinsic.translation[0],calib->calibrations[i].extrinsic.translation[1],calib->calibrations[i].extrinsic.translation[2]);
                LOG_DEBUG("R%d:%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf",i,
                          calib->calibrations[i].extrinsic.rotation[0],calib->calibrations[i].extrinsic.rotation[1],calib->calibrations[i].extrinsic.rotation[2],
                          calib->calibrations[i].extrinsic.rotation[3],calib->calibrations[i].extrinsic.rotation[4],calib->calibrations[i].extrinsic.rotation[5],
                          calib->calibrations[i].extrinsic.rotation[6],calib->calibrations[i].extrinsic.rotation[7],calib->calibrations[i].extrinsic.rotation[8]);
                LOG_DEBUG("K%d:%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf}",i+1,
                          calib->calibrations[i].intrinsic.K[0],calib->calibrations[i].intrinsic.K[1],calib->calibrations[i].intrinsic.K[2],
                          calib->calibrations[i].intrinsic.K[3],calib->calibrations[i].intrinsic.K[4],calib->calibrations[i].intrinsic.K[5],
                          calib->calibrations[i].intrinsic.K[6],calib->calibrations[i].intrinsic.K[7],calib->calibrations[i].intrinsic.K[8],
                          calib->calibrations[i].intrinsic.K[9],calib->calibrations[i].intrinsic.K[10]);
            }
            return true;
        }
    }
    return false;
}

extern "C" JNIEXPORT void JNICALL
Java_org_xvisio_xvsdk_DeviceWatcher_nAddUsbDevice(JNIEnv *env, jclass type, jstring deviceName_,
                                                     jint fileDescriptor) {
    //const char *deviceName = env->GetStringUTFChars(deviceName_, 0);
    //LOG_DEBUG("AddUsbDevice, adding device: %s, descriptor: %d", std::string(deviceName).c_str(), fileDescriptor );

    static bool firstCall = true;
    if( firstCall ) {
        firstCall = false;
        std::cout.rdbuf(new androidout);
        std::cerr.rdbuf(new androiderr);
        env->GetJavaVM(&jvm);
        std::cout << "Initialized" << std::endl;
    }

    int fd = fileDescriptor;
    LOG_DEBUG("fileDescriptor: %d", fd);

    xv::setLogLevel(xv::LogLevel::debug);

    LOG_DEBUG("xv::getDevice");
    device = xv::getDevice(fd);
    auto info = device->info();

    LOG_DEBUG("device.reset");
    device.reset();

    std::string hw_version = info["hardwareVersion"];
    LOG_DEBUG("hardware version: %s", hw_version.c_str());

    LOG_DEBUG("xv::getDevice");
    device = xv::getDevice(fd);

    if (device->colorCamera()) {
        device->colorCamera()->registerCallback( [](xv::ColorImage const & rgb){
            jvm->AttachCurrentThread(&s_rgbEnv, NULL);

            if (s_rgbCallback == nullptr && s_XCameraClass && s_rgbEnv) {
                s_rgbCallback = s_rgbEnv->GetStaticMethodID(s_XCameraClass, "rgbCallback", "(II[I)V");
            }

            if (s_rgbCallback && s_XCameraClass && s_rgbEnv) {

                int w = rgb.width;
                int h = rgb.height;
                int s = w * h;

                auto d = rgb.data.get();

                jintArray data = s_rgbEnv->NewIntArray(s);
                jint *body = s_rgbEnv->GetIntArrayElements(data, 0);

                yuv2rgb((unsigned char *)d, body, w, h);

                s_rgbEnv->CallStaticVoidMethod(s_XCameraClass, s_rgbCallback, w, h, data);

                s_rgbEnv->DeleteLocalRef(data);
            }
        });
        device->colorCamera()->start();
    } else {
        std::cout << "No RGB camera.\n";
    }


    if (device->imuSensor()) {
        device->imuSensor()->registerCallback([](xv::Imu const & imu){
            jvm->AttachCurrentThread(&s_imuEnv, NULL);

            if (s_imuCallback == nullptr && s_XCameraClass ) {
                s_imuCallback = s_imuEnv->GetStaticMethodID(s_XCameraClass, "imuCallback", "(DDD)V");
            }

            if (s_imuCallback && s_XCameraClass ) {
                s_imuEnv->CallStaticVoidMethod(s_XCameraClass, s_imuCallback, static_cast<double>(imu.accel[0]), static_cast<double>(imu.accel[1]), static_cast<double>(imu.accel[2]));
            }
        });
    }
    if (device->tofCamera())
    {
        device->tofCamera()->registerCallback([](xv::DepthImage const &im){
            //LOG_DEBUG("eddy tof type: %d", im.type);
            std::shared_ptr<const xv::DepthImage> tmp = std::make_shared<xv::DepthImage>(im);;
            unsigned srcWidth = tmp->width;
            unsigned srcHeight = tmp->height;
            double distance = 4.5;


            if (s_tofEnv == nullptr) {
                jvm->AttachCurrentThread(&s_tofEnv, NULL);
            }
            if (s_tofCallback == nullptr && s_XCameraClass ) {
                s_tofCallback = s_tofEnv->GetStaticMethodID(s_XCameraClass, "tofCallback", "(II[I)V");
            }

            if (s_tofCallback && s_XCameraClass && s_tofEnv) {

                int w = im.width;
                int h = im.height;
                int s = w * h;
                float *f = new float[srcWidth * srcHeight];
                memcpy(f, tmp->data.get(), tmp->dataSize);
                cv::Mat m(srcHeight, srcWidth, CV_32FC1, f);
                cv::Mat map, mask, mask0, mask1, mask2;
                cv::threshold(m, map, 10.5, 0, cv::THRESH_TOZERO_INV);
                cv::threshold(m, mask, 0.2, 1.0, cv::THRESH_BINARY);
                mask.convertTo(mask0, CV_8UC1, 255, 0);
                cv::Mat adjMap;
                map.convertTo(adjMap, CV_8UC1, 255.0 / std::max(0.01, distance), 0);
                cv::Mat falseColorsMap(m.rows, m.cols, CV_8UC3);
                cv::applyColorMap(adjMap, falseColorsMap, cv::COLORMAP_RAINBOW);
                falseColorsMap.copyTo(adjMap, mask0);


                jintArray data = s_tofEnv->NewIntArray(s);

                jint *body = s_tofEnv->GetIntArrayElements(data, 0);

                cv::Mat mrgb(srcHeight, srcWidth, CV_8UC4, body);
                cv::cvtColor(adjMap, mrgb, cv::COLOR_BGR2RGBA);

                s_tofEnv->CallStaticVoidMethod(s_XCameraClass, s_tofCallback, w, h, data);

                s_tofEnv->DeleteLocalRef(data);
            }
        });
    }
    device->tofCamera()->start();
    if (device->fisheyeCameras()) {
        device->fisheyeCameras()->registerCallback([](xv::FisheyeImages const & stereo){
            jvm->AttachCurrentThread(&s_stereoEnv, NULL);

            if (s_stereoCallback == nullptr && s_XCameraClass ) {
                s_stereoCallback = s_stereoEnv->GetStaticMethodID(s_XCameraClass, "stereoCallback", "(II[I)V");
            }

            if (s_stereoCallback && s_XCameraClass ) {

                int w = stereo.images[0].width;
                int h = stereo.images[0].height;
                int s = w * h;

                auto d = stereo.images[0].data.get();

                jintArray data = s_stereoEnv->NewIntArray(s);
                jint *body = s_stereoEnv->GetIntArrayElements(data, 0);

                for (int i = 0; i < w; i++) {
                    for (int j = 0; j < h; j++) {
                        auto v = d[(w - i - 1) + (h - j - 1) * w];
                        body[i + j * w] =
                                0xFF000000 + (v << 16 & 0xFF0000) + (v << 8 & 0xFF00) + (v & 0xFF);
                    }
                }

                s_stereoEnv->CallStaticVoidMethod(s_XCameraClass, s_stereoCallback, w, h, data);

                s_stereoEnv->DeleteLocalRef(data);
            }
        });
        device->fisheyeCameras()->start();
    }

    if (device->slam()) {
        device->slam()->registerCallback([](const xv::Pose& pose){
            jvm->AttachCurrentThread(&s_poseEnv2, NULL);

            if (s_poseCallback == nullptr && s_XCameraClass ) {
                s_poseCallback = s_poseEnv2->GetStaticMethodID(s_XCameraClass, "poseCallback", "(DDDDDD)V");
            }

            if (s_poseCallback && s_XCameraClass ) {
                auto pitchYawRoll = xv::rotationToPitchYawRoll(pose.rotation());
                s_poseEnv2->CallStaticVoidMethod(s_XCameraClass, s_poseCallback, static_cast<double>(pose.x()), static_cast<double>(pose.y()), static_cast<double>(pose.z()),
                                                 static_cast<double>(pitchYawRoll[0]), static_cast<double>(pitchYawRoll[1]), static_cast<double>(pitchYawRoll[2]));
            }
        });
        device->slam()->start();
    }
    usleep(1000*1000);
    m_ready = true;
    LOG_DEBUG("device->slam()->start() finished");
}

extern "C" JNIEXPORT void JNICALL
Java_org_xvisio_xvsdk_DeviceWatcher_nRemoveUsbDevice(JNIEnv *env, jclass type,
                                                        jint fileDescriptor) {

}

extern "C" JNIEXPORT void JNICALL
Java_org_xvisio_xvsdk_XCamera_initCallbacks(JNIEnv *env, jclass type) {

    s_XCameraClass = reinterpret_cast<jclass>(env->NewGlobalRef(type));
}

extern "C" JNIEXPORT void JNICALL
Java_org_xvisio_xvsdk_DeviceWatcher_nSetSlamMode(JNIEnv *env, jclass type,
                                                     jint mode) {
    LOG_DEBUG("switch to mode %d", mode);
    device->slam()->stop();
    if (mode == 0) {
        device->slam()->start(xv::Slam::Mode::Mixed);
    } else if (mode == 1) {
        device->slam()->start(xv::Slam::Mode::Edge);
    } else if (mode == 2) {
        device->slam()->start(xv::Slam::Mode::EdgeFusionOnHost);
    }
}

static long int crv_tab[256];
static long int cbu_tab[256];
static long int cgu_tab[256];
static long int cgv_tab[256];
static long int tab_76309[256];
static unsigned char clp[1024];   //for clip in CCIR601

void init_yuv420p_table()
{
    long int crv,cbu,cgu,cgv;
    int i,ind;

    crv = 104597; cbu = 132201;  /* fra matrise i global.h */
    cgu = 25675;  cgv = 53279;

    for (i = 0; i < 256; i++)
    {
        crv_tab[i] = (i-128) * crv;
        cbu_tab[i] = (i-128) * cbu;
        cgu_tab[i] = (i-128) * cgu;
        cgv_tab[i] = (i-128) * cgv;
        tab_76309[i] = 76309*(i-16);
    }

    for (i = 0; i < 384; i++)
        clp[i] = 0;
    ind = 384;
    for (i = 0;i < 256; i++)
        clp[ind++] = i;
    ind = 640;
    for (i = 0;i < 384; i++)
        clp[ind++] = 255;
}

void yuv2rgb(unsigned char* yuvbuffer, int* rgbbuffer, int width, int height ){

    int y1, y2, u, v;
    unsigned char *py1, *py2;
    int i, j, c1, c2, c3, c4;
    int *d1, *d2;
    unsigned char *src_u, *src_v;
    static int init_yuv420p = 0;

    src_u = yuvbuffer + width * height;   // u
    src_v = src_u + width * height / 4;  // v

    py1 = yuvbuffer;   // y
    py2 = py1 + width;
    d1 = rgbbuffer;
    d2 = d1 + width;

    if (init_yuv420p == 0)
    {
        init_yuv420p_table();
        init_yuv420p = 1;
    }

    for (j = 0; j < height; j += 2)
    {
        for (i = 0; i < width; i += 2)
        {
            u = *src_u++;
            v = *src_v++;

            c1 = crv_tab[v];
            c2 = cgu_tab[u];
            c3 = cgv_tab[v];
            c4 = cbu_tab[u];

            //up-left
            y1 = tab_76309[*py1++];
            int A = 255;
            int R = clp[384+((y1 + c1)>>16)];
            int G = clp[384+((y1 - c2 - c3)>>16)];
            int B = clp[384+((y1 + c4)>>16)];
            *d1++ = (A & 0xff) << 24 | (R & 0xff) << 16 | (G & 0xff) << 8 | (B & 0xff);

            //down-left
            y2 = tab_76309[*py2++];
            R = clp[384+((y2 + c1)>>16)];
            G = clp[384+((y2 - c2 - c3)>>16)];
            B = clp[384+((y2 + c4)>>16)];
            *d2++ = (A & 0xff) << 24 | (R & 0xff) << 16 | (G & 0xff) << 8 | (B & 0xff);

            //up-right
            y1 = tab_76309[*py1++];
            R = clp[384+((y1 + c1)>>16)];
            G = clp[384+((y1 - c2 - c3)>>16)];
            B = clp[384+((y1 + c4)>>16)];
            *d1++ = (A & 0xff) << 24 | (R & 0xff) << 16 | (G & 0xff) << 8 | (B & 0xff);

            //down-right
            y2 = tab_76309[*py2++];
            R = clp[384+((y2 + c1)>>16)];
            G = clp[384+((y2 - c2 - c3)>>16)];
            B = clp[384+((y2 + c4)>>16)];
            *d2++ = (A & 0xff) << 24 | (R & 0xff) << 16 | (G & 0xff) << 8 | (B & 0xff);
        }
        d1  += width;
        d2  += width;
        py1 += width;
        py2 += width;
    }
}
