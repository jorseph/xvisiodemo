LOCAL_PATH := $(call my-dir)

# --------------------------------------------------------------------

include $(CLEAR_VARS)
LOCAL_MODULE := usb1.0
LOCAL_SRC_FILES := ../../libs/$(TARGET_ARCH_ABI)/libusb1.0.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := xvuvc
LOCAL_SRC_FILES := ../../libs/$(TARGET_ARCH_ABI)/libxvuvc.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := jpeg
LOCAL_SRC_FILES := ../../libs/$(TARGET_ARCH_ABI)/libjpeg-turbo1500.so
include $(PREBUILT_SHARED_LIBRARY)


include $(CLEAR_VARS)
LOCAL_MODULE := octomap
LOCAL_SRC_FILES := ../../libs/$(TARGET_ARCH_ABI)/liboctomap.so
include $(PREBUILT_SHARED_LIBRARY)


include $(CLEAR_VARS)
LOCAL_MODULE := xslam-usb-sdk
LOCAL_SRC_FILES := ../../libs/$(TARGET_ARCH_ABI)/libxslam-usb-sdk.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := xslam-hid-sdk
LOCAL_SRC_FILES := ../../libs/$(TARGET_ARCH_ABI)/libxslam-hid-sdk.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := xslam-uvc-sdk
LOCAL_SRC_FILES := ../../libs/$(TARGET_ARCH_ABI)/libxslam-uvc-sdk.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := xslam-vsc-sdk
LOCAL_SRC_FILES := ../../libs/$(TARGET_ARCH_ABI)/libxslam-vsc-sdk.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := xslam-edge-sdk
LOCAL_SRC_FILES := ../../libs/$(TARGET_ARCH_ABI)/libxslam-edge-sdk.so
include $(PREBUILT_SHARED_LIBRARY)


include $(CLEAR_VARS)
LOCAL_MODULE := xslam-slam_core
LOCAL_SRC_FILES := ../../libs/$(TARGET_ARCH_ABI)/libxslam_core.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)
LOCAL_MODULE := xslam-slam_lib-ange
LOCAL_SRC_FILES := ../../libs/$(TARGET_ARCH_ABI)/libxslam_libange.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := xslam-slam_surface-reconstruction
LOCAL_SRC_FILES := ../../libs/$(TARGET_ARCH_ABI)/libxslam_surfacereconstruction.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := xslam_algo_sdk
LOCAL_SRC_FILES := ../../libs/$(TARGET_ARCH_ABI)/libxslam_algo_sdk.so
LOCAL_SHARED_LIBRARIES := xslam_core
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := opencv_core_so
LOCAL_SRC_FILES := ../../libs/$(TARGET_ARCH_ABI)/libopencv_core.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := opencv_imgproc_so
LOCAL_SRC_FILES := ../../libs/$(TARGET_ARCH_ABI)/libopencv_imgproc.so
include $(PREBUILT_SHARED_LIBRARY)


include $(CLEAR_VARS)
LOCAL_MODULE := xslam-xv-sdk
LOCAL_SRC_FILES := ../../libs/$(TARGET_ARCH_ABI)/libxslam-xv-sdk.so
include $(PREBUILT_SHARED_LIBRARY)


# --------------------------------------------------------------------

include $(CLEAR_VARS)

examples = \
	slam_edge_example \
	slam_example \
	tof_example \

$(foreach EXAMPLE, $(examples),\
  $(eval include $(LOCAL_PATH)/example.mk) \
)

exampledirs = \
	slam_3dof_display_calib \
	all_stream \

$(foreach EXAMPLE, $(exampledirs),\
  $(eval include $(LOCAL_PATH)/exampledir.mk) \
)

include $(LOCAL_PATH)/demo.mk

