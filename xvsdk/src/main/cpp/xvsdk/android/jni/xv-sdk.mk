LOCAL_PATH_XV_SDK := $(call my-dir)
LOCAL_PATH_XV_SDK_SRC := $(LOCAL_PATH_XV_SDK)/../../src

LOCAL_PATH:=$(LOCAL_PATH_XV_SDK)
ROOT_REL:= ../..
ROOT_ABS:= $(LOCAL_PATH)/../..

src_dir=../../

version := 1.0.0
commit := $(shell ($(LOCAL_PATH)/get_commit.sh $(LOCAL_PATH)))
version := $(version)-$(commit)

version := $(version)
$(info version:=$(version))

$(shell sed 's/@PROJECT_VERSION@/$(version)/g' $(ROOT_ABS)/src/version.h.in > $(ROOT_ABS)/src/version.h)

include $(CLEAR_VARS)

ifeq ($(filter $(modules-get-list),xslam-usb-sdk),)
    include $(LOCAL_PATH_XV_SDK_SRC)/../usb_sdk/android/jni/Android.mk
endif

ifeq ($(filter $(modules-get-list),xslam-hid-sdk),)
    include $(LOCAL_PATH_XV_SDK_SRC)/../hid_sdk/android/jni/Android.mk
endif

ifeq ($(filter $(modules-get-list),xslam-uvc-sdk),)
    include $(LOCAL_PATH_XV_SDK_SRC)/../uvc_sdk/android/jni/Android.mk
endif

ifeq ($(filter $(modules-get-list),xslam-vsc-sdk),)
    include $(LOCAL_PATH_XV_SDK_SRC)/../vsc_sdk/android/jni/Android.mk
endif

ifeq ($(filter $(modules-get-list),xslam-edge-sdk),)
    include $(LOCAL_PATH_XV_SDK_SRC)/../edge_sdk/android/jni/Android.mk
endif

#ifeq ($(filter $(modules-get-list),xslam-edge-plus-sdk),)
#    include $(LOCAL_PATH_XV_SDK_SRC)/../edge_plus_sdk/android/jni/Android.mk
#endif

ifeq ($(filter $(modules-get-list),xslam-slam-sdk),)
    include $(LOCAL_PATH_XV_SDK_SRC)/../xslam_sdk/android/jni/Android.mk
endif

#third_party=../../../../../3rd-party-android/
#LOCAL_MODULE := usb1.0
#LOCAL_SRC_FILES := $(third_party)/lib/usb1.0/$(TARGET_PLATFORM)/$(TARGET_ARCH_ABI)/libusb1.0.so
#include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)

LOCAL_PATH=$(LOCAL_PATH_XV_SDK)

LOCAL_SRC_FILES := \
    $(ROOT_REL)/src/device_p.cpp \
    $(ROOT_REL)/src/device-private.cpp \
    $(ROOT_REL)/src/edge.cpp \
    $(ROOT_REL)/src/logger.cpp \
    $(ROOT_REL)/src/slam.cpp \
    $(ROOT_REL)/src/slam_loader.cpp \
    $(ROOT_REL)/src/slam-vision-only.cpp \
    $(ROOT_REL)/src/time-server.cpp \
    $(ROOT_REL)/src/xv-sdk.cpp \
    $(ROOT_REL)/src/xv-types.cpp \
    $(ROOT_REL)/src/imu3dof/dynamic_calibration_storage.cpp \
    $(ROOT_REL)/src/imu3dof/imu_3dof.cpp \
    $(ROOT_REL)/src/imu3dof/look_up_table.cpp \
    $(ROOT_REL)/src/slam-algo-convert.cpp \
    $(ROOT_REL)/src/edge-fusion-on-host.cpp \


LOCAL_C_INCLUDES := \
    bionic \
    framework/libs/base/include \
    $(LOCAL_PATH_XV_SDK_SRC)/../xslam_sdk/third-party/prebuild/include/ \
    $(LOCAL_PATH_XV_SDK_SRC)/../include/ \
    $(LOCAL_PATH_USB_SDK_SRC)/ \
    $(LOCAL_PATH_HID_SDK_SRC)/ \
    $(LOCAL_PATH_UVC_SDK_SRC)/ \
    $(LOCAL_PATH_VSC_SDK_SRC)/ \
    $(LOCAL_PATH_EDGE_SDK_SRC)/ \
    $(LOCAL_PATH_XV_SDK_SRC)/../third-lib/boost/ \
    $(LOCAL_PATH_XV_SDK_SRC)/../third-lib/spdlog/include/ \
    $(LOCAL_PATH_XV_SDK_SRC)/../third-lib/eigen/

LOCAL_EXPORT_C_INCLUDES += \
    $(ROOT_ABS)/ \
    $(ROOT_ABS)/include


LOCAL_MODULE := xslam-xv-sdk

LOCAL_SHARED_LIBRARIES := xslam-hid-sdk xslam-uvc-sdk xslam-vsc-sdk xslam-edge-sdk xslam-usb-sdk xslam_algo_sdk


LOCAL_CPPFLAGS := -O3 -fPIC -std=c++11 -DNDEBUG
LOCAL_CFLAGS := -O3 -fPIC -DNDEBUG
LOCAL_CFLAGS += -std=gnu99
LOCAL_LDLIBS := -lz
#LOCAL_LDFLAGS := -lpthread

include $(BUILD_SHARED_LIBRARY)

# --------------------------------------------------------------------

#xv_examples = \
#    3dof \
#    plane_stream

#$(foreach MODULE, $(xv_examples),\
#  $(eval include $(LOCAL_PATH_XV_SDK)/examples.mk) \
#)

#include $(LOCAL_PATH_XV_SDK)/demo.mk
