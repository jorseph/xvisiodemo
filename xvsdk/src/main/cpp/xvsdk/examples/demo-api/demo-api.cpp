#include <xv-sdk.h>

#include <cstdlib>
#include <cstring>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <cmath>
#include <thread>
#include <chrono>
#include <atomic>
#include <iterator>
#include <mutex>

#include "fps_count.hpp"
#include "pipe_srv.h"

std::string map_filename = "map.bin";
std::string map_shared_filename = "map_shared.bin";
std::atomic_int localized_on_reference_percent(0);
std::filebuf mapStream;

void cam_sig_handler(int sig)
{
	switch(sig) {
	case SIGINT:
		printf("catch SIGINT\n");
		break;
	case SIGHUP:
		printf("catch SIGHUB\n");
		break;
	case SIGTERM:
		printf("catch SIGTERM\n");
		break;
	}
	vsc_client_pipe_terminal_srv();
    exit(1);
}

void imuCallback(std::shared_ptr<const xv::Imu> imu)
{
    static FpsCount fc;
    fc.tic();
    static int k=0;
    if(k++%100==0){
        std::cout << "imu" << "@" << std::round(fc.fps()) << "fps"
            << " Time=(" << imu->edgeTimestampUs << " " << imu->hostTimestamp << "),"
            << " Gyro=(" << imu->gyro[0] << " " << imu->gyro[1] << " " << imu->gyro[2] << "),"
            << " Accel=("  << imu->accel[0] << " " << imu->accel[1] << " " << imu->accel[2] << "),"
            << " Temperature=" << imu->temperature
            << std::endl;
    }
}

//add event callback
void eventCallback(xv::Event const & event)
{
    std::string strType = "Unknown";
    std::string strEvent = "Unknown";
    switch (event.type)
    {
    case 0x01:
        strType = "key1";
        if(event.state == 0x02) strEvent = "trigger";
        break;
    case 0x06:
        strType = "als";
        strEvent = std::to_string(event.state);
        break;
    case 0x07:
        strType = "tp";
        if(event.state == 0x00) strEvent = "none";
        else if(event.state == 0x03) strEvent = "single-click";
        else if(event.state == 0x04) strEvent = "double-click";
        else if(event.state == 0x05) strEvent = "right-slip";
        else if(event.state == 0x06) strEvent = "left-slip";
        else if(event.state == 0x07) strEvent = "down-slip";
        else if(event.state == 0x08) strEvent = "up-slip";
        break;
    case 0x09:
        strType = "key2";
        if(event.state == 0x02) strEvent = "trigger";
        break;
    default:
        break;
    }
    std::cout<<"****Event@"<<"edgeTimestampUs:"<<event.edgeTimestampUs
    <<";  Type:"<<strType<<";  State:"<<strEvent<<std::endl;
}

//add CNN callback
void cnnCallback(std::vector<xv::Object> objs)
{
    static int k=0;
    if(k++%5==0){
        for(int i = 0; i < objs.size(); i++)
        {
            xv::Object obj = objs.at(i);
            std::cout<<obj.width<<"x"<<obj.height<<" # "<<obj.type<<" # confidence:"<<obj.confidence<<"; "<<std::endl;
        }
    }
}

void orientationCallback(xv::Orientation const & o)
{
    static FpsCount fc;
    fc.tic();
    static int k=0;
    if(k++%100==0){
        auto& q= o.quaternion();
        std::cout << "orientation" << "@" << std::round(fc.fps()) << "fps"
            << " 3dof=("  << q[0] << " " << q[1] << " " << q[2] << " " << q[3] << "),"
            << std::endl;
    }
}


void eyetrackingCallback(xv::EyetrackingImage const & o)
{
    static FpsCount fc;
    fc.tic();
    static int k=0;
    if(k++%30==0){
        std::cout << "[eyetracking]" << o.images[0].width << "x" << o.images[0].height << "@" << std::round(fc.fps()) << "fps"
            << std::endl;
    }
}

void stereoCallback(std::shared_ptr<const xv::FisheyeImages> stereo)
{
    static FpsCount fc;
    fc.tic();
    static int k = 0;
    if(k++%50==0){
        std::cout << stereo->images[0].width << "x" << stereo->images[0].height + stereo->images[1].height << "@" << std::round(fc.fps()) << "fps" << std::endl;
    }
}

void poseCallback(xv::Pose const& pose){
    static FpsCount fc;
    fc.tic();
    static int k=0;
    if(k++%100==0){
        auto t = pose.translation();
        auto r = xv::rotationToPitchYawRoll(pose.rotation());
        std::cout << "slam-callback-pose" << "@" << std::round(fc.fps()) << "fps"
            << " p=(" << t[0] << " " << t[1] << " " << t[2]
            << " ), r=(" << r[0] << " " << r[1] << " " << r[2] << " )"
            << ", Confidence= " << pose.confidence()
            << std::endl;
    }
}

std::thread tpos;
bool stop = false;
double t = -1;

void stopGetPose()
{
    stop = true;
    tpos.join();
}
void startGetPose(std::shared_ptr<xv::Slam> slam)
{
    stop = false;
    tpos = std::thread([&]{
        double prediction = 0.005;

        long n = 0;
        long nb_ok = 0;
        xv::Pose pose;
        while (!stop) {
          std::this_thread::sleep_for( std::chrono::milliseconds(2) );

          bool ok = slam->getPose( pose, prediction );

          if ( ok ) {
              ++nb_ok;
              static int k=0;
              if(k++%100==0){
                  auto t = pose.translation();
                  auto r = xv::rotationToPitchYawRoll(pose.rotation());

                  //std::cout << "slam-get-pose [" << p->x << "," << p->y << "," << p->z << "]" << std::endl;
                  std::cout << std::setprecision(5) << "slam-get-pose"
                  << " p=(" << t[0] << " " << t[1] << " " << t[2]
                  << " ), r=(" << r[0] << " " << r[1] << " " << r[2] << " )"
                  << ", Confidence= " << pose.confidence()
                  << std::endl;
              }
          }
          n++;
        }
        std::cout << "Nb get pose ok: " << 100.0*double(nb_ok)/n << "% (" << nb_ok << "/" << n << ")" << std::endl;
    });

}

void startGetPoseAt(std::shared_ptr<xv::Slam> slam)
{
    std::cout << "Wait 5s ...\n";
    std::this_thread::sleep_for(std::chrono::seconds(5));
    stop = false;
    tpos = std::thread ([&]{
        while(!stop)
        {
            auto now = std::chrono::steady_clock::now();

            t = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::steady_clock::now().time_since_epoch()).count();
            t += 0.0123;
            xv::Pose poseAt;
            if (slam->getPoseAt(poseAt, t)) {
                static int k = 0;
                if(k++%100==0){
                    auto pitchYawRoll = xv::rotationToPitchYawRoll(poseAt.rotation());
                    std::cout << "slam-poseAt" << " (" << poseAt.x() << "," << poseAt.y() << "," << poseAt.z() << "," << pitchYawRoll[0]*180/M_PI << "," << pitchYawRoll[1]*180/M_PI << "," << pitchYawRoll[2]*180/M_PI << ")" << std::endl;
                }
            }

            // to simulate the 60Hz loop
            std::this_thread::sleep_until(now+std::chrono::microseconds(long(1./60.*1e6)));
        }
        
    });
}

xv::Orientation orientation30ms;
xv::Orientation orientationAt;

void Start3DofGet(std::shared_ptr<xv::OrientationStream> orientation)
{
    stop = false;
    tpos = std::thread ([&]{
        while(!stop)
        {
            auto now = std::chrono::steady_clock::now();
            if (orientation->get(orientation30ms, 0.030)) {
                static int k = 0;
                if(k++%100==0){
                    auto& q= orientation30ms.quaternion();
                    std::cout << "orientation30ms"
                        << " 3dof=("  << q[0] << " " << q[1] << " " << q[2] << " " << q[3] << "),"
                        << std::endl;
                }
            }
            // to simulate the 60Hz loop
            std::this_thread::sleep_until(now+std::chrono::microseconds(long(1./60.*1e6)));
        }
        
    });
}

void Stop3DofGet()
{
    stop = true;
    if(tpos.joinable())
    {
        tpos.join();
    }   
}

void Start3DofGetAt(std::shared_ptr<xv::OrientationStream> orientation)
{
    stop = false;
    tpos = std::thread ([&]{
        while(!stop)
        {
            auto now = std::chrono::steady_clock::now();
            t = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::steady_clock::now().time_since_epoch()).count() * 1e-6;
            t += 0.0123;
            if (orientation->getAt(orientationAt, t)) {
                static int k = 0;
                if(k++%100==0){
                    auto& q= orientationAt.quaternion();
                    std::cout << "orientationAt"
                        << " 3dof=("  << q[0] << " " << q[1] << " " << q[2] << " " << q[3] << "),"
                        << std::endl;
                }
            }
            // to simulate the 60Hz loop
            std::this_thread::sleep_until(now+std::chrono::microseconds(long(1./60.*1e6)));
        }
        
    });
}

void Stop3DofGetAt()
{
    stop = true;
    if(tpos.joinable())
    {
        tpos.join();
    }   
}

void cslamSavedCallback(int status_of_saved_map, int map_quality)
{
    std::cout << " Save map (quality is " << map_quality << "/100) and switch to CSlam:";
    switch(status_of_saved_map)
    {
    case  2: std::cout << " Map well saved. " << std::endl; break;
    case -1: std::cout << " Map cannot be saved, an error occured when trying to save it." << std::endl; break;
    default: std::cout << " Unrecognized status of saved map " << std::endl; break;
    }
    mapStream.close();
}

void cslamSwitchedCallback(int map_quality)
{
    std::cout << " map (quality is " << map_quality << "/100) and switch to CSlam:";
    mapStream.close();
}

void  fisheyeLCallback(xv::FisheyeImages const & fisheye){
    static FpsCount fc;
    fc.tic();
    static int k = 0;
    if(k++%50==0 && fisheye.images.size() >= 1){
        std::cout << fisheye.images.at(0).width << "x" << fisheye.images.at(0).height << "@" << std::round(fc.fps()) << "fps" << std::endl;
    }
}

void  fisheyeRCallback(xv::FisheyeImages const & fisheye){
    static FpsCount fc;
    fc.tic();
    static int k = 0;
    if(k++%50==0 && fisheye.images.size() >= 2){
        std::cout << fisheye.images.at(1).width << "x" << fisheye.images.at(1).height << "@" << std::round(fc.fps()) << "fps" << std::endl;
    }
}

void rgbCallback( xv::ColorImage const & rgb){
    static FpsCount fc;
    fc.tic();
    static int k = 0;
    if(k++%10==0){
        std::cout << rgb.width << "x" << rgb.height << "@" << std::round(fc.fps()) << "fps" << std::endl;
    }
}

void tofCallback(xv::DepthImage const & tof) {
    std::string types[] = { "Depth_16", "Depth_32", "IR", "Cloud", "Raw", "Eeprom" };
    static FpsCount fc;
    fc.tic();
    static int k;
    if (k++ % 10 == 0) {
        std::cout << tof.width << "x" << tof.height << "@" << std::round(fc.fps()) << "fps" << std::endl;
    }
}

void planeCallback(std::shared_ptr<const std::vector<xv::Plane>> planes)
{
    if(planes)
    {
        for (auto const& plane : *planes.get()) {
            static int k;
            if (k++ % 20 == 0) {
                std::cout << "got plane, id: " << plane.id << ", [" << plane.normal[0] << "," << plane.normal[1] << "," << plane.normal[2] << "]" << std::endl;
                for(auto const& x: plane.points)
                    std::cout << "(" << x[0] << "," << x[1] << " " << x[2] << ")";
                std::cout << std::endl;
            }
        }
    }
}

template<class F, std::size_t N>
std::ostream& operator<<(std::ostream& o, const std::array<F,N> &v)
{
    o << "[";
    for(int i=0;i<N;i++){
        o << v.at(i);
        if( i<N-1){
            o << ", ";
        }
    }
    o << "]";
    return o;
}
std::ostream& operator<<(std::ostream& o, const xv::UnifiedCameraModel &m)
{
    o << "{";
    o << "w=" << m.w << ", ";
    o << "h=" << m.h << ", ";
    o << "fx=" << m.fx << ", ";
    o << "fy=" << m.fy << ", ";
    o << "u0=" << m.u0 << ", ";
    o << "v0=" << m.v0 << ", ";
    o << "xi=" << m.xi;
    o << "}";
    return o;
}

std::ostream& operator<<(std::ostream& o, const xv::PolynomialDistortionCameraModel &m)
{
    o << "{";
    o << "w=" << m.w << ", ";
    o << "h=" << m.h << ", ";
    o << "fx=" << m.fx << ", ";
    o << "fy=" << m.fy << ", ";
    o << "u0=" << m.u0 << ", ";
    o << "v0=" << m.v0 << ", ";
    o << "distor=" << m.distor;
    o << "}";
    return o;
}

std::ostream& operator<<(std::ostream& o, xv::Calibration const& c)
{
    o << "Calibration:" << std::endl;
    o << " R:" << c.pose.rotation() << std::endl;
    o << " T: " << c.pose.translation() << std::endl;
    for(int i=0;i<c.ucm.size();i++){
        o << "UCM" << i << ": " << c.ucm.at(i) << std::endl;
    }
    for(int i=0;i<c.pdcm.size();i++){
        o << "PDCM" << i << ": " << c.pdcm.at(i) << std::endl;
    }
    return o;
}

std::ostream& operator<<(std::ostream& o, const std::vector<xv::Calibration> &calibs)
{
    for( auto c: calibs){
        std::cout << c << std::endl;
    }
    return o;
}

void SetRgbResolution(int cmd, std::shared_ptr<xv::ColorCamera> camera)
{
    switch (cmd)
    {
    case 0:
        camera->setResolution(xv::ColorCamera::Resolution::RGB_1920x1080);
        break;
    case 1:
        camera->setResolution(xv::ColorCamera::Resolution::RGB_1280x720);
        break;
    case 2:
        camera->setResolution(xv::ColorCamera::Resolution::RGB_640x480);
        break;
    case 3:
        camera->setResolution(xv::ColorCamera::Resolution::RGB_320x240);
        break;
    case 4:
        camera->setResolution(xv::ColorCamera::Resolution::RGB_2560x1920);
        break;
    default:
        std::cout << "set rgb resolution format wrong" << std::endl;
        return;
        break;
    }
    std::cout << "set rgb resolution successfully" << std::endl;
}

bool SetTofDistanceMode(int cmd, std::shared_ptr<xv::TofCamera> camera)
{
    bool bOk = false;
    switch (cmd)
    {
    case 0:
        bOk = camera->setDistanceMode(xv::TofCamera::DistanceMode::Short);
        break;
    case 1:
        bOk = camera->setDistanceMode(xv::TofCamera::DistanceMode::Middle);
        break;
    case 2:
        bOk = camera->setDistanceMode(xv::TofCamera::DistanceMode::Long);
        break;
    default:
        break;
    }
    return bOk;
}

bool SetTofStreamMode(int cmd, std::shared_ptr<xv::TofCamera> camera)
{
    bool bOk = false;
    switch (cmd)
    {
    case 0:
        bOk = camera->setStreamMode(xv::TofCamera::StreamMode::DepthOnly);
        break;
    case 1:
        bOk = camera->setStreamMode(xv::TofCamera::StreamMode::CloudOnly);
        break;
    case 2:
        bOk = camera->setStreamMode(xv::TofCamera::StreamMode::DepthAndCloud);
        break;
    case 3:
        bOk = camera->setStreamMode(xv::TofCamera::StreamMode::None);
        break;
    default:
        break;
    }
    return bOk;
}

void cslamLocalizedCallback(float percent)
{
    static int k=0;
    if(k++%100==0){
        localized_on_reference_percent = static_cast<int>(percent*100);
        std::cout << "localized: " << localized_on_reference_percent << "%" << std::endl;
    }
}

int main( int argc, char* argv[] ) try
{
    // may change the log level this way :
    xv::setLogLevel(xv::LogLevel::debug);

    std::string json = "";
    if (argc == 2) {
        std::ifstream ifs( argv[1] );
        if( !ifs.is_open() ){
            std::cerr << "Failed to open: " << argv[1] << std::endl;
            return -1;
        }

        std::stringstream fbuf;
        fbuf << ifs.rdbuf();
        json = fbuf.str();
    }

    static const char meun_main[]={
        "\n\nDemo\n"
        "------------------------------\n"
        "1 : Init SDK and get IMU\n"
        "2 : Stop get IMU, start slam, get 6dof\n"
        "3 : Stop slam, stop get 6dof, get IMU\n"
        "4 : Get eyetracking data, stop get IMU\n"
        "5 : Stop get eyetracking data, get IMU\n"
        "9 : Stop get IMU, get rgb data\n"
        "10: Stop rgb, get IMU\n"
        "11: Stop get IMU, get tof data\n"
        "12: Stop tof, get IMU\n"
        "13: Start tof plane detection, start slam, stop get IMU, get 6dof\n"
        "14: Stop tof plane detection, stop slam, get IMU\n"
        "15: Stop get IMU, switch to edge mode, get edge 6dof\n"
        "16: Stop get edge 6dof, switch to mixed mode, get IMU\n"
        "17: Test display: open display,set brightness to 2,set brightness to 9\n"
        "18: Test display: close display\n"
        "19: Stop get IMU, start slam, get 6dof\n"
        "20: Save shared map\n"
        "21: Stop slam, start CSLAM using shared map, start slam\n"
        "22: Stop slam, get IMU\n"
        "23: Start stereo plane detection, start slam, stop get IMU, get 6dof\n"
        "24: Stop stereo plane detection, stop slam, get IMU\n"
        "25: Stop get IMU, switch to EdgeFusionOnHost mode, get 6dof with callback\n"
        "26: Stop get IMU, switch to EdgeFusionOnHost mode, get 6dof with get-pose\n"
        "27: Call 3Dof get()\n"
        "28: Call 3Dof getAt()\n"
        "29: Stop get IMU, switch to mixed mode, get 6dof with callback\n"
        "30: Stop get IMU, switch to mixed mode, get 6dof with get-pos-at\n"
        "31: Stop get IMU, read rgb calibration\n"
        "32: Stop get IMU, set rgb resolution\n"
        "33: Stop get IMU, set rbg format\n"
        "34: Stop get IMU, set rbg exposure mode, white balance control, ISO level, EXP level\n"
        "35: Stop get IMU, set eyetracking exposure, gain\n"
        "36: Stop get IMu, set eyetracking led control\n"
        "37: Stop get IMU, read fisheye calibration\n"
        "38: Stop get IMU, read TOF calibration\n"
        "39: Stop get IMU, set TOF stream mode\n"
        "40: Stop get IMU, set TOF distance mode\n"
        "41: Stop get IMU, set event data\n"
        "42: Stop get IMU, set CNN data\n"
        "0 : exit program\n"
        "------------------------------\n"
        "enter select:"
    };

    int retval;
    int cond = 1;
    int cmd;
    int lastcmd = 0;
    int recvcnt = 0;

    retval = vsc_client_pipe_init();
    if (retval != 0) {
        printf("client pipe init fail %d\n", retval);
        return retval;
    }
    vsc_client_pipe_get_srv_pid();
    signal(SIGINT, cam_sig_handler);

    int cnnId = -1;
    int eventId = -1;
    int iFisheyeLId = -1;
    int iFisheyeRId = -1;
    int imuId = -1;
    int poseId = -1;
    int rgbId = -1;
    int tofId = -1;
    int planeId = -1;
    int stereoId = -1;
    int eyetrackingId = -1;

    std::shared_ptr<xv::Device> device = nullptr;

    while (cond) {
        cmd = vsc_client_pipe_request_cmd(meun_main, sizeof(meun_main));
        if (cmd == 0) {
            printf("program will exit\n");
            cond = 0;
            break;
        } /*else if (lastcmd == cmd) {
            recvcnt++;
            if (recvcnt>5) {
                printf("client lose disconnect \n");
                //cond = 0;
            }
        }*/
        lastcmd = cmd;

        printf("cmd: %d\n", cmd);
        switch (cmd) {
        case 1:
            {
                // return a map of devices with serial number as key, wait at most x seconds if no device detected
                auto devices = xv::getDevices(10., json);

                // if no device: quit
                if (devices.empty()) {
                    std::cerr << "Timeout for device detection." << std::endl;
                    return EXIT_FAILURE;
                }

                // take the first device in the map
                device = devices.begin()->second;

                if (!device->slam()) {
                    std::cerr << "Host SLAM algorithm not supported." << std::endl;
                    return EXIT_FAILURE;
                }
            }

            // get IMU
            device->orientationStream()->start();
            imuId = device->orientationStream()->registerCallback( orientationCallback );

            break;
        case 2:
        case 19:
            // Stop get IMU
            device->orientationStream()->unregisterCallback( imuId );

            // start mix slam
            device->slam()->start(xv::Slam::Mode::Mixed);

            // get mixed 6dof
            startGetPose(device->slam());

            break;
        case 3 :
        case 22:
            // Stop get mixed 6dof
            stopGetPose();

            // stop mix slam
            device->slam()->stop();

            // get IMU
            imuId = device->orientationStream()->registerCallback( orientationCallback );

            break;
        case 4 :
            // Stop get IMU
            device->orientationStream()->unregisterCallback( imuId );

            // Get eyetracking data
            device->eyetracking()->start();
            eyetrackingId = device->eyetracking()->registerCallback( eyetrackingCallback );

            break;
        case 5 :
            // Stop get eyetracking data
            device->eyetracking()->stop();
            device->eyetracking()->unregisterCallback( eyetrackingId );

            // get IMU
            imuId = device->orientationStream()->registerCallback( orientationCallback );

            break;
        case 9 :
            // Stop get IMU
            device->orientationStream()->unregisterCallback( imuId );

            // Get rgb data
            device->colorCamera()->start();
            rgbId = device->colorCamera()->registerCallback( rgbCallback );

            break;
        case 10:
            // Stop get rgb data
            device->colorCamera()->stop();
            device->colorCamera()->unregisterCallback( rgbId );

            // get IMU
            imuId = device->orientationStream()->registerCallback( orientationCallback );

            break;
        case 11:
            // Stop get IMU
            device->orientationStream()->unregisterCallback( imuId );

            // Get tof data
            device->tofCamera()->start();
            tofId = device->tofCamera()->registerCallback( tofCallback );

            break;
        case 12:
            // Stop get tof data
            device->tofCamera()->stop();
            device->tofCamera()->unregisterCallback( tofId );

            // get IMU
            imuId = device->orientationStream()->registerCallback( orientationCallback );

            break;
        case 13:
            // Stop get IMU
            device->orientationStream()->unregisterCallback( imuId );

            // Get plane data
            device->tofCamera()->start();
            // !!! Must call registerTofPlanesCallback before slam()->start
            planeId = device->slam()->registerTofPlanesCallback( planeCallback );

            // start mix slam
            device->slam()->start(xv::Slam::Mode::Mixed);

            // get mixed 6dof
            startGetPose(device->slam());

            break;
        case 14:
            // stop mix slam
            device->slam()->stop();

            // Stop get mixed 6dof
            stopGetPose();

            // Stop get plane data
            device->tofCamera()->stop();
            device->slam()->unregisterTofPlanesCallback( planeId );

            // get IMU
            imuId = device->orientationStream()->registerCallback( orientationCallback );

            break;
        case 15:
            // Stop get IMU
            device->orientationStream()->unregisterCallback( imuId );

            // switch to edge mode
            device->slam()->start(xv::Slam::Mode::Edge);

            // get edge 6dof
            poseId = device->slam()->registerCallback( poseCallback );

            break;
        case 16:
            // stop get edge 6dof
            device->slam()->unregisterCallback( poseId );

            // switch to mixed mode
            device->slam()->stop();

            // get IMU
            imuId = device->orientationStream()->registerCallback( orientationCallback );

            break;
        case 17:
            device->display()->open();
            std::this_thread::sleep_for(std::chrono::seconds(3));
            device->display()->setBrightnessLevel(2);
            std::this_thread::sleep_for(std::chrono::seconds(3));
            device->display()->setBrightnessLevel(9);
            break;
        case 18:
            device->display()->close();
            break;
        case 20:
            // save shared map
            //slam->stopSlamAndSaveMap(map_shared_filename);
            if (mapStream.open(map_filename, std::ios::binary|std::ios::out|std::ios::trunc) == nullptr) {
                std::cout << "open " << map_filename << " failed." << std::endl;
                break;
            }
            device->slam()->saveMapAndSwitchToCslam(mapStream,  cslamSavedCallback, cslamLocalizedCallback);

            break;
        case 21:
            // stop mix slam
            std::cout << "stop slam" << std::endl;
            device->slam()->stop();

            //std::this_thread::sleep_for( std::chrono::seconds(1) );

            // start mix slam
            std::cout << "start slam" << std::endl;
            device->slam()->start(xv::Slam::Mode::Mixed);

            // start cslam using shared map
            std::cout << "load cslam map and switch to cslam" << std::endl;
            if (mapStream.open(map_filename, std::ios::binary|std::ios::in) == nullptr) {
                std::cout << "open " << map_filename << " failed." << std::endl;
                break;
            }
            device->slam()->loadMapAndSwitchToCslam(
                mapStream,
                cslamSwitchedCallback,
                cslamLocalizedCallback
                );

            break;
        case 23:
            // Stop get IMU
            device->orientationStream()->unregisterCallback( imuId );

            // start mix slam
            device->slam()->start(xv::Slam::Mode::Mixed);

            // Get plane data
            planeId = device->slam()->registerStereoPlanesCallback( planeCallback );

            // get mixed 6dof
            startGetPose(device->slam());

            break;
        case 24:
            // stop mix slam
            device->slam()->stop();

            // Stop get mixed 6dof
            stopGetPose();

            // Stop get plane data
            device->slam()->unregisterStereoPlanesCallback( planeId );

            // get IMU
            imuId = device->orientationStream()->registerCallback( orientationCallback );

            break;
        case 25:
            // Stop get IMU
            device->orientationStream()->unregisterCallback( imuId );

            // switch to EdgeFusionOnHost mode
            device->slam()->start(xv::Slam::Mode::EdgeFusionOnHost);

            // get EdgeFusionOnHost 6dof with callback
            poseId = device->slam()->registerCallback( poseCallback );
        case 26:
            // Stop get IMU
            device->orientationStream()->unregisterCallback( imuId );

            // start EdgeFusionOnHost slam
            device->slam()->start(xv::Slam::Mode::EdgeFusionOnHost);

            // get EdgeFusionOnHost 6dof with get-pose
            startGetPose(device->slam());

            break;
        case 27:
            // Stop get IMU
            device->orientationStream()->unregisterCallback( imuId );

            //stop getAt 3dof
            Stop3DofGetAt();

            //call get 3dof
            Start3DofGet(device->orientationStream());
            break;
        case 28:
            // Stop get IMU
            device->orientationStream()->unregisterCallback( imuId );

            //stop get 3dof
            Stop3DofGet();

            //call getAt 3dof
            Start3DofGetAt(device->orientationStream());
            break;
        case 29:
            // Stop get IMU
            device->orientationStream()->unregisterCallback( imuId );

            // switch to Mixed mode
            device->slam()->start(xv::Slam::Mode::Mixed);

            // get Mixed 6dof with callback
            poseId = device->slam()->registerCallback( poseCallback );
            break;
        case 30:
            // Stop get IMU
            device->orientationStream()->unregisterCallback( imuId );

            if (device->fisheyeCameras()) {
                device->fisheyeCameras()->registerCallback([](xv::FisheyeImages const & images) {});
            }

            // switch to Mixed mode
            device->slam()->start(xv::Slam::Mode::Mixed);

            // get Mixed 6dof with get-pos-at
            startGetPoseAt(device->slam());
            break;
        case 31 :
            // Stop get IMU
            device->orientationStream()->unregisterCallback( imuId );
            device->orientationStream()->stop();

            // Stop get rgb data
            device->colorCamera()->stop();
            device->colorCamera()->unregisterCallback( rgbId );

            // Get rgb calibration
            std::cout << "RGB calibration:" << std::endl;
            std::cout << device->colorCamera()->calibration() << std::endl;

            break;
        case 32 :
            {
                // Stop get IMU
                device->orientationStream()->unregisterCallback( imuId );
                device->orientationStream()->stop();

                // Stop get rgb data
                device->colorCamera()->stop();
                device->colorCamera()->unregisterCallback( rgbId );

                const char meun_rgbResolution[]={
                    "0: RGB_1920x1080   ///< RGB 1080p\n"
                    "1: RGB_1280x720    ///< RGB 720p\n"
                    "2: RGB_640x480     ///< RGB 480p\n"
                    "3: RGB_320x240     ///< RGB QVGA(not supported now)\n"
                    "4: RGB_2560x1920   ///< RGB 5m(not supported now)\n"
                    "------------------------------\n"
                    "enter select:"
                };

                int nRgbControlCmd = -1;
                
                nRgbControlCmd = vsc_client_pipe_request_cmd(meun_rgbResolution, sizeof(meun_rgbResolution));
                SetRgbResolution(nRgbControlCmd, device->colorCamera());
            
                // Get rgb data
                device->colorCamera()->start();
                rgbId = device->colorCamera()->registerCallback( rgbCallback );
            }
            break;
        case 33 :
            {
                // Stop get IMU
                device->orientationStream()->unregisterCallback( imuId );
                device->orientationStream()->stop();

                // Stop get rgb data
                device->colorCamera()->stop();
                device->colorCamera()->unregisterCallback( rgbId );

                int nRgbFormat = -1;

                xv::DeviceSetting setting = {0x03030046};

                const char meun_rgbFormat[]={
                    "0: YUYV\n"
                    "1: YUV420p\n"
                    "2: JPEG\n"
                    "3: NV12\n"
                    "------------------------------\n"
                    "enter select:"
                };
                
                nRgbFormat = vsc_client_pipe_request_cmd(meun_rgbFormat, sizeof(meun_rgbFormat));   

                setting.args.val[0] = nRgbFormat + 1;

                bool bOk = device->control(setting);
                if(bOk)
                {
                    std::cout << "RGB format setting successfully" << std::endl;
                }
                else{
                    std::cout << "RGB format setting failed" << std::endl;
                }
            }
            break;
        case 34 :
            {
                // Stop get IMU
                device->orientationStream()->unregisterCallback( imuId );
                device->orientationStream()->stop();

                // Stop get rgb data
                device->colorCamera()->stop();
                device->colorCamera()->unregisterCallback( rgbId );

                xv::DeviceSetting setting = {0x00000000};
                int nRgbControl = -1, nExpMode = -1, nAWB = -1, nISO = -1, nEXP = -1;

                const char meun_rgbControl[]={
                    "0: exposure mode\n"
                    "1: white balance\n"
                    "2: ISO level\n"
                    "3: EXP level\n"
                    "------------------------------\n"
                    "enter select:"
                };

                nRgbControl = vsc_client_pipe_request_cmd(meun_rgbControl, sizeof(meun_rgbControl));

                switch (nRgbControl)
                {
                case 0:
                    {
                        const char meun_rgbExpMode[]={
                            "0: auto exposure\n"
                            "1: manual exposure\n"
                            "------------------------------\n"
                            "enter select:"
                        };
                        nExpMode = vsc_client_pipe_request_cmd(meun_rgbExpMode, sizeof(meun_rgbExpMode));
                        setting = {0x03030005};
                        setting.args.exp.exp_mode = nExpMode;
                    }
                    break;
                case 1:
                    {
                        const char meun_rgbAWB[]={
                        "0: AWB_OFF\n"
                        "1: AWB_AUTO\n"
                        "2: AWB_INCAN\n"
                        "3: AWB_FLOUR\n"
                        "4: AWB_WARM_FLOUR\n"
                        "5: AWB_DAYLIGHT\n"
                        "6: AWB_CLOUDY\n"
                        "7: AWB_TWILIGHT\n"
                        "8: AWB_SHADOW\n"
                        "------------------------------\n"
                        "enter select:"
                        };
                        nAWB = vsc_client_pipe_request_cmd(meun_rgbAWB, sizeof(meun_rgbAWB));
                        setting = {0x03030007};
                        setting.args.awb.awb_mode = nAWB;
                    }
                    break;
                case 2:
                    {
                        const char meun_rgbISO[]={
                        "iso level: 1-16 (100-1600)\n"
                        "------------------------------\n"
                        "enter select:"
                        };
                        nISO = vsc_client_pipe_request_cmd(meun_rgbISO, sizeof(meun_rgbISO));
                        setting = {0x03030022};
                        setting.args.exp.iso_mode = nISO;
                    }
                    break;
                case 3:
                    {
                        const char meun_rgbEXP[]={
                        "exp level: -9 ~ 9"
                        "------------------------------\n"
                        "enter select:"
                        };
                        nEXP = vsc_client_pipe_request_cmd(meun_rgbEXP, sizeof(meun_rgbEXP));
                        setting = {0x03030006};
                        setting.args.exp.exp_level = nEXP;
                    }
                    break;
                default:
                    break;
                }
                bool bOk = device->control(setting);
                if(bOk)
                {
                    std::cout << "RGB setting successfully" << std::endl;
                }
                else{
                    std::cout << "RGB setting failed" << std::endl;
                }

            }
            break;
        case 35:
            {
                // Stop get IMU
                device->orientationStream()->unregisterCallback( imuId );
                device->orientationStream()->stop();

                int nLeftGain = -1, nRightGain = -1, nLeftTime = -1, nRightTime = -1;
                const char meun_EyeTrackingLeftGain[]={
                        "Left eye exposure gain, [0, 255]\n"
                        "------------------------------\n"
                        "enter select:"
                        };
                nLeftGain = vsc_client_pipe_request_cmd(meun_EyeTrackingLeftGain, sizeof(meun_EyeTrackingLeftGain));

                const char meun_EyeTrackingLeftTime[] = {
                        "Left eye exposure time, in milliseconds\n"
                        "------------------------------\n"
                        "enter select:"
                        };
                nLeftTime = vsc_client_pipe_request_cmd(meun_EyeTrackingLeftTime, sizeof(meun_EyeTrackingLeftTime));

                const char meun_EyeTrackingRightGain[]={
                        "Right eye exposure gain, [0, 255]\n"
                        "------------------------------\n"
                        "enter select:"
                        };
                nRightGain = vsc_client_pipe_request_cmd(meun_EyeTrackingRightGain, sizeof(meun_EyeTrackingRightGain));

                const char meun_EyeTrackingRightTime[] = {
                        "Right eye exposure time, in milliseconds\n"
                        "------------------------------\n"
                        "enter select:"
                        };
                nRightTime = vsc_client_pipe_request_cmd(meun_EyeTrackingRightTime, sizeof(meun_EyeTrackingRightTime));

                bool bOk = device->eyetracking()->setExposure(nLeftGain,nLeftTime,nRightGain,nRightTime);
                if(bOk)
                {
                    std::cout << "Eyetracking exposure setting successfully" << std::endl;
                }
                else{
                    std::cout << "Eyetracking exposure setting failed" << std::endl;
                }
            }
            break;
        case 36:
            {
                // Stop get IMU
                device->orientationStream()->unregisterCallback( imuId );
                device->orientationStream()->stop();

                int nEye = -1, nLed = -1, nBright = -1;
                const char meun_EyeTrackingLed[]={
                        "0: left\n"
                        "1: right\n"
                        "2: both\n"
                        "------------------------------\n"
                        "enter select:"
                        };
                nEye = vsc_client_pipe_request_cmd(meun_EyeTrackingLed, sizeof(meun_EyeTrackingLed));

                const char meun_EyeTrackingLedCtrl[]={
                        "[0,7]:led index, 8:all\n"
                        "------------------------------\n"
                        "enter select:"
                        };
                nLed = vsc_client_pipe_request_cmd(meun_EyeTrackingLedCtrl, sizeof(meun_EyeTrackingLedCtrl));

                const char meun_EyeTrackingBright[]={
                        "[0,255], 0 is off\n"
                        "------------------------------\n"
                        "enter select:"
                        };
                nBright = vsc_client_pipe_request_cmd(meun_EyeTrackingBright, sizeof(meun_EyeTrackingBright));


                bool bOk = device->eyetracking()->setLedBrighness(nEye,nLed,nBright);
                if(bOk)
                {
                    std::cout << "Eyetracking setting successfully" << std::endl;
                }
                else{
                    std::cout << "Eyetracking setting failed" << std::endl;
                }
            }
            break;
        case 37 :
            // Stop get IMU
            device->orientationStream()->unregisterCallback( imuId );
            device->orientationStream()->stop();

            // Stop get Fisheye data
            device->fisheyeCameras()->stop();
            device->fisheyeCameras()->unregisterCallback( rgbId );

            // Get Fisheye calibration
            std::cout << "Fisheye calibration:" << std::endl;
            std::cout << device->fisheyeCameras()->calibration() << std::endl;
            break;
        case 38 :
            // Stop get IMU
            device->orientationStream()->unregisterCallback( imuId );
            device->orientationStream()->stop();

            // Stop get TOF data
            device->tofCamera()->stop();

            // Get TOF calibration
            std::cout << "TOF calibration:" << std::endl;
            std::cout << device->tofCamera()->calibration() << std::endl;
            break;
        case 39:
            {
                // Stop get IMU
                device->orientationStream()->unregisterCallback( imuId );
                device->orientationStream()->stop();

                // Stop get TOF
                device->tofCamera()->stop();
                
                int nStreamMode = -1;
                const char meun_TOFStreamMode[]={
                        "0: DepthOnly\n"
                        "1: CloudOnly\n"
                        "2: DepthAndCloud\n"
                        "3: none\n"
                        "------------------------------\n"
                        "enter select:"
                        };
                nStreamMode = vsc_client_pipe_request_cmd(meun_TOFStreamMode, sizeof(meun_TOFStreamMode));
                
                bool bOk = SetTofStreamMode(nStreamMode, device->tofCamera());
                if(bOk)
                {
                    std::cout << "Tof distance setting successfully" << std::endl;
                }
                else{
                    std::cout << "Tof distance setting failed" << std::endl;
                }
            }
            break;
        case 40:
            {
                // Stop get IMU
                device->orientationStream()->unregisterCallback( imuId );
                device->orientationStream()->stop();

                // Stop get TOF
                device->tofCamera()->stop();

                int nDisMode = -1;
                const char meun_TOFDistance[]={
                        "0: short\n"
                        "1: middle(Midlle=Short for 010/009 TOF)\n"
                        "2: long\n"
                        "------------------------------\n"
                        "enter select:"
                        };
                nDisMode = vsc_client_pipe_request_cmd(meun_TOFDistance, sizeof(meun_TOFDistance));
                
                bool bOk = SetTofDistanceMode(nDisMode, device->tofCamera());
                if(bOk)
                {
                    std::cout << "Tof distance setting successfully" << std::endl;
                }
                else{
                    std::cout << "Tof distance setting failed" << std::endl;
                }
            }
            break;
        case 41 :
        {
            if(device == nullptr)
            {
                std::cerr<<"Device is nullptr! Please init first..."<<std::endl;
                break;
            }
            device->eventStream()->unregisterCallback(imuId);

            if(device->orientationStream()){
                device->orientationStream()->unregisterCallback( imuId );
                device->orientationStream()->stop();
            }
            // Stop get rgb data if colorCamera is
            if(device->colorCamera())
            {
                device->colorCamera()->unregisterCallback( rgbId );
                device->colorCamera()->stop();
            }
            if(device->imuSensor())
            {
                device->imuSensor()->unregisterCallback( imuId);
                device->imuSensor()->stop();
            }
            // get event data
            device->eventStream()->start();
            eventId = device->eventStream()->registerCallback( eventCallback );

            const char alsMode[]={
                "0: commen event\n"
                "1: als\n"
                "2: exit\n"
                "------------------------------\n"
                "enter select:"
            };
            bool isExit = false;
            // int iAlsMode = vsc_client_pipe_request_cmd(alsMode, sizeof(alsMode));
            while(!isExit)
            {
                int iAlsMode = vsc_client_pipe_request_cmd(alsMode, sizeof(alsMode));
                std::vector<unsigned char> vecWrite;
                std::vector<unsigned char> vecRead;
                bool ret;
                //open als
                if(iAlsMode == 1)
                {
                    vecWrite.push_back(0x02);
                    vecWrite.push_back(0xFD);
                    vecWrite.push_back(0x67);
                    vecWrite.push_back(0x00);
                    ret = device->hidWriteAndRead(vecWrite, vecRead);  
                    if(!ret)
                        std::cerr<<"open als error!"<<std::endl;
                    else{
                        std::cout<<"open als success!"<<std::endl;
                        std::cout<<"read data: "<<std::endl;
                        for(int i = 0; i < vecRead.size(); i++)
                        {
                            std::cout<<std::hex<<(unsigned int)(unsigned char)vecRead.at(i)<<" ";
                        }
                        std::cout<<std::endl;
                    }
                    //recover event catch
                    device->eventStream()->stop();
                    device->eventStream()->start();
                }else if(iAlsMode == 2)
                {
                    isExit = true;
                    vecWrite.push_back(0x02);
                    vecWrite.push_back(0xFD);
                    vecWrite.push_back(0x67);
                    vecWrite.push_back(0x01);
                    ret = device->hidWriteAndRead(vecWrite, vecRead);  
                    if(!ret)
                        std::cerr<<"close als error!"<<std::endl;
                    else{
                        std::cout<<"close als success!"<<std::endl;
                    }
                    //recover event catch
                    device->eventStream()->stop();
                    device->eventStream()->start();
                    device->eventStream()->unregisterCallback(eventId);
                }
            }
            break;
        }
        case 42 :
        {
            if(device == nullptr)
            {
                std::cerr<<"Device is nullptr! Please init first..."<<std::endl;
                break;
            }
            if(device->orientationStream()){
                device->orientationStream()->unregisterCallback( imuId );
                device->orientationStream()->stop();
            }
            if (!device->objectDetector()) 
            {
                std::cerr << "No object detector" << std::endl;
                break;
            }
            cnnId = device->objectDetector()->registerCallback(cnnCallback);
            device->objectDetector()->start();

            if(!device->objectDetector()->setModel("data/down/CNN_2x8x_r14_5.blob"))
                std::cerr<<"***setModel error!"<<std::endl;
            if(!device->objectDetector()->setDescriptor("data/down/config_tensorflow.json"))
                std::cerr<<"***setDescriptor error!"<<std::endl;

            const char meunCamera[]={
                "1: camera source recognition\n"
                "2: switch to Left fisheye\n"
                "3: switch to Right fisheye\n"
                "4: switch to GRB\n"
                "5: switch to TOF\n"
                "0: Exit\n"
                "------------------------------\n"
                "enter select:"
            };
            bool isExit = false;
            while(!isExit)
            {
                int cameraCtrl = vsc_client_pipe_request_cmd(meunCamera, sizeof(meunCamera));
                switch (cameraCtrl){
                case 1:
                {
                    xv::ObjectDetector::Source source = device->objectDetector()->getSource();
                    if(source == xv::ObjectDetector::Source::LEFT)
                    std::cout<<"-----------------camera source: LEFT-----------------"<<std::endl;
                    else if(source == xv::ObjectDetector::Source::RIGHT)
                    std::cout<<"-----------------camera source: RIGHT-----------------"<<std::endl;
                    else if(source == xv::ObjectDetector::Source::RGB)
                    std::cout<<"-----------------camera source: RGB-----------------"<<std::endl;
                    else if(source == xv::ObjectDetector::Source::TOF)
                    std::cout<<"-----------------camera source: TOF-----------------"<<std::endl;
                    break;
                }
                case 2:
                    if(device->colorCamera()){
                            device->colorCamera()->stop();
                    }
                    if(device->tofCamera()){
                        device->tofCamera()->stop();
                    }
                    if(device->objectDetector()){
                        device->objectDetector()->setSource(xv::ObjectDetector::Source::LEFT);
                    }
                    if (device->fisheyeCameras()) {
                        device->fisheyeCameras()->unregisterCallback(iFisheyeLId);
                        device->fisheyeCameras()->unregisterCallback(iFisheyeRId);
                        iFisheyeLId = device->fisheyeCameras()->registerCallback(fisheyeLCallback);
                        device->fisheyeCameras()->start();
                    }
                    break;
                case 3:
                    if(device->colorCamera()){
                        device->colorCamera()->stop();
                    }
                    if(device->tofCamera()){
                        device->tofCamera()->stop();
                    }
                    if(device->objectDetector()){
                        device->objectDetector()->setSource(xv::ObjectDetector::Source::RIGHT);
                    }
                    if (device->fisheyeCameras()) {
                        device->fisheyeCameras()->unregisterCallback(iFisheyeLId);
                        device->fisheyeCameras()->unregisterCallback(iFisheyeRId);
                        iFisheyeRId = device->fisheyeCameras()->registerCallback(fisheyeRCallback);
                        device->fisheyeCameras()->start();
                    }
                    break;
                case 4:
                    if (device->fisheyeCameras()) {
                        device->fisheyeCameras()->stop();
                        device->fisheyeCameras()->unregisterCallback(iFisheyeLId);
                        device->fisheyeCameras()->unregisterCallback(iFisheyeRId);
                    }
                    if(device->tofCamera()){
                        device->tofCamera()->stop();
                    }
                    if(device->objectDetector()){
                        device->objectDetector()->setSource(xv::ObjectDetector::Source::RGB);
                    }
                    if(device->colorCamera()){
                        device->colorCamera()->unregisterCallback(rgbId);
                        rgbId = device->colorCamera()->registerCallback( rgbCallback );
                        device->colorCamera()->start();
                    }
                    break;
                case 5:
                    if (device->fisheyeCameras()) {
                        device->fisheyeCameras()->stop();
                        device->fisheyeCameras()->unregisterCallback(iFisheyeLId);
                        device->fisheyeCameras()->unregisterCallback(iFisheyeRId);
                    }
                    if(device->colorCamera()){
                            device->colorCamera()->stop();
                    }
                    if(device->objectDetector()){
                        device->objectDetector()->setSource(xv::ObjectDetector::Source::TOF);
                    }
                    if(device->tofCamera()){
                        device->tofCamera()->unregisterCallback(tofId);
                        tofId = device->tofCamera()->registerCallback( tofCallback );
                        device->tofCamera()->start();
                    }
                    break;
                case 0:
                {
                    if(device->colorCamera()){
                        device->colorCamera()->stop();
                        device->colorCamera()->unregisterCallback(rgbId);
                    }
                    if(device->tofCamera()){
                        device->tofCamera()->stop();
                        device->tofCamera()->unregisterCallback(tofId);
                    }
                    if (device->fisheyeCameras()) {
                        device->fisheyeCameras()->stop();
                        device->fisheyeCameras()->unregisterCallback(iFisheyeLId);
                        device->fisheyeCameras()->unregisterCallback(iFisheyeRId);
                    }
                    if(device->objectDetector()){
                        device->objectDetector()->stop();
                        device->objectDetector()->unregisterCallback(cnnId);
                    }
                    isExit = true;
                    break;
                }
                default:
                    break;
                }
            }
        }
        default:
            printf("bad command\n");
            break;
        }
    }
    stop = true;
    if (tpos.joinable()) {
        tpos.join();
    }

    vsc_client_pipe_terminal_srv();

	return EXIT_SUCCESS;
}
catch( const std::exception &e){
    std::cerr << e.what() << std::endl;
    return EXIT_FAILURE;
}
