#define _USE_MATH_DEFINES

#include <cstdlib>
#include <thread>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>
#include <mutex>
#include <signal.h>

#include <xv-sdk.h>
#include "colors.h"

//#define USE_EX
//#define USE_PRIVATE

bool s_stop = false;

#ifdef USE_EX
#include "xv-sdk-ex.h"
#endif
#ifdef USE_PRIVATE
#include "xv-sdk-private.h"
#endif

#ifdef USE_OPENCV
#include <opencv2/opencv.hpp>

cv::Mat raw_to_opencv( std::shared_ptr<const xv::ColorImage> rgb);
cv::Mat raw_to_opencv( std::shared_ptr<const xv::DepthImage> tof);
cv::Mat raw_to_opencv( std::shared_ptr<const xv::GrayScaleImage> tof_ir);
std::pair<cv::Mat,cv::Mat> raw_to_opencv( std::shared_ptr<const xv::FisheyeImages> stereo);
cv::Mat raw_to_opencv( std::shared_ptr<const xv::DepthColorImage> rgbd);

#ifdef USE_EX
std::pair<cv::Mat,cv::Mat> raw_to_opencv(std::shared_ptr<const xv::FisheyeImages> stereo, std::shared_ptr<const xv::FisheyeKeyPoints<2,32>> keypoints, std::shared_ptr<const std::vector<std::pair<int, std::array<xv::Vector2d, 4>>>> tags)
{
    cv::Mat left = cv::Mat::zeros(400, 640, CV_8UC1);
    cv::Mat right = cv::Mat::zeros(400, 640, CV_8UC1);

    if (stereo) {
        auto const& leftInput = stereo->images[0];
        auto const& rightInput = stereo->images[1];
        if( leftInput.data != nullptr ){
            std::memcpy(left.data, leftInput.data.get(), static_cast<size_t>(left.rows*left.cols));
        }
        if( rightInput.data != nullptr ){
            std::memcpy(right.data, rightInput.data.get(), static_cast<size_t>(right.rows*right.cols));
        }
    }

    cv::cvtColor(left, left, cv::COLOR_GRAY2BGR);
    cv::cvtColor(right, right, cv::COLOR_GRAY2BGR);

    if (keypoints) {
        const int size = 2;
        int s=0;
        for(unsigned int i=0; i<keypoints->descriptors[s].size; i++ ){
            auto p = keypoints->descriptors[s].keypoints.get() + i*2;
            cv::Point pt(*p, *(p+1));
            cv::line(left, pt - cv::Point(size,0), pt + cv::Point(size,0), cv::Scalar(0,0,255) );
            cv::line(left, pt - cv::Point(0,size), pt + cv::Point(0,size), cv::Scalar(0,0,255) );
        }
        s=1;
        for(unsigned int i=0; i<keypoints->descriptors[s].size; i++ ){
            auto p = keypoints->descriptors[s].keypoints.get() + i*2;
            cv::Point pt(*p, *(p+1));
            cv::line(right, pt - cv::Point(size,0), pt + cv::Point(size,0), cv::Scalar(0,0,255) );
            cv::line(right, pt - cv::Point(0,size), pt + cv::Point(0,size), cv::Scalar(0,0,255) );
        }
    }

    if (tags) {
        for(auto const& t : *tags){
            auto const& pts = t.second;
            std::size_t i=0;
            cv::line(left, cv::Point(pts[i][0],pts[i][1]), cv::Point(pts[i+1][0],pts[i+1][1]), cv::Scalar(0,255,0) ); i++;
            cv::line(left, cv::Point(pts[i][0],pts[i][1]), cv::Point(pts[i+1][0],pts[i+1][1]), cv::Scalar(0,255,0) ); i++;
            cv::line(left, cv::Point(pts[i][0],pts[i][1]), cv::Point(pts[i+1][0],pts[i+1][1]), cv::Scalar(0,255,0) ); i++;
            cv::line(left, cv::Point(pts[i][0],pts[i][1]), cv::Point(pts[0][0],pts[0][1]), cv::Scalar(0,255,0) );

            cv::putText(left, std::to_string(t.first),
                        cv::Point(pts[i][0], pts[i][1]), //top-left position
                        cv::FONT_HERSHEY_DUPLEX,
                        0.25,
                        CV_RGB(0, 255, 0), //font color
                        1.);
        }
    }

    return {left, right};
}


#endif


std::shared_ptr<const xv::ColorImage> s_rgb = nullptr;
std::shared_ptr<const xv::DepthImage> s_tof = nullptr;
std::shared_ptr<const xv::GrayScaleImage> s_ir = nullptr;
std::shared_ptr<const xv::FisheyeImages> s_stereo = nullptr;
std::shared_ptr<const xv::DepthColorImage> s_depthColor = nullptr;

#ifdef USE_EX
std::shared_ptr<const xv::FisheyeKeyPoints<2,32>> s_keypoints = nullptr;
std::mutex s_mtx_tags;
std::shared_ptr<const std::vector<std::pair<int, std::array<xv::Vector2d, 4>>>> s_tags;
#endif

std::mutex s_mtx_rgb;
std::mutex s_mtx_tof;
std::mutex s_mtx_depthColor;
std::mutex s_mtx_ir;
std::mutex s_mtx_stereo;

void display() {
    cv::namedWindow("Left");
    cv::moveWindow("Left", 20, 20);
    cv::namedWindow("Right");
    cv::moveWindow("Right", 660, 20);
    cv::namedWindow("RGB");
    cv::moveWindow("RGB", 20, 462);
    cv::namedWindow("TOF");
    cv::moveWindow("TOF", 500, 462);
    cv::namedWindow("IR");
    cv::moveWindow("IR", 500 + 640, 462);
    cv::waitKey(1);

    while( !s_stop ){
        std::shared_ptr<const xv::ColorImage> rgb = nullptr;
        std::shared_ptr<const xv::DepthImage> tof = nullptr;
        std::shared_ptr<const xv::GrayScaleImage> ir = nullptr;
        std::shared_ptr<const xv::FisheyeImages> stereo = nullptr;
#ifdef USE_EX
        std::shared_ptr<const xv::FisheyeKeyPoints<2,32>> keypoints = nullptr;
        std::shared_ptr<const std::vector<std::pair<int, std::array<xv::Vector2d, 4>>>> tags;
#endif

        s_mtx_stereo.lock();
        stereo = s_stereo;
#ifdef USE_EX
        keypoints = s_keypoints;
        s_mtx_tags.lock();
        tags = s_tags;
        s_mtx_tags.unlock();
#endif
        s_mtx_stereo.unlock();

#ifdef USE_EX
        auto imgs = raw_to_opencv(stereo, keypoints, tags);
        cv::imshow("Left", imgs.first);
        cv::imshow("Right", imgs.second);
#else
        if (stereo) {
            auto imgs = raw_to_opencv(stereo);
            cv::imshow("Left", imgs.first);
            cv::imshow("Right", imgs.second);
        }
#endif
        s_mtx_rgb.lock();
        rgb = s_rgb;
        s_mtx_rgb.unlock();
        if (rgb && rgb->width>0 && rgb->height>0) {
            cv::Mat img = raw_to_opencv(rgb);
            cv::resize(img, img, cv::Size(), 0.25, 0.25);
            cv::imshow("RGB", img);
        }
        s_mtx_tof.lock();
        tof = s_tof;
        s_mtx_tof.unlock();
        if (tof) {
            cv::Mat img = raw_to_opencv(tof);
            if (img.rows>0 && img.cols>0)
                cv::imshow("TOF", img);
        }
        s_mtx_depthColor.lock();
        auto depthColor = s_depthColor;
        s_mtx_depthColor.unlock();
        if (depthColor) {
            cv::Mat img = raw_to_opencv(depthColor);
            if (img.rows>0 && img.cols>0)
                cv::imshow("RGBD (depth)", img);
        }
        s_mtx_ir.lock();
        ir = s_ir;
        s_mtx_ir.unlock();
        if (ir) {
            cv::Mat img = raw_to_opencv(ir);
            if (img.rows>0 && img.cols>0)
                cv::imshow("IR", img);
        }
        cv::waitKey(1);
    }
}
#endif

#include "fps_count.hpp"

std::string timeShowStr(std::int64_t edgeTimestampUs, double hostTimestamp) {
    char s[1024];
    double now = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::steady_clock::now().time_since_epoch()).count()*1e-6;
    std::sprintf(s, " (device=%lld host=%.4f now=%.4f delay=%.4f) ", (long long)edgeTimestampUs, hostTimestamp, now, now-hostTimestamp);
    return std::string(s);
}
std::string timeShowStr(double hostTimestamp) {
    char s[1024];
    double now = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::steady_clock::now().time_since_epoch()).count()*1e-6;
    std::sprintf(s, " (host=%.4f now=%.4f delay=%.4f) ", hostTimestamp, now, now-hostTimestamp);
    return std::string(s);
}

int main( int argc, char* argv[] ) try
{
    std::cout << "xvsdk version: " << xv::version() << std::endl;

    xv::setLogLevel(xv::LogLevel::debug);

    std::string json = "";
    if (argc == 2) {
        std::ifstream ifs( argv[1] );
        if( !ifs.is_open() ){
            std::cerr << "Failed to open: " << argv[1] << std::endl;
            return -1;
        }

        std::stringstream fbuf;
        fbuf << ifs.rdbuf();
        json = fbuf.str();
    }

    auto devices = xv::getDevices(10., json);

    if (devices.empty()) {
        std::cout << "Timeout: no device found\n";
        return EXIT_FAILURE;
    }



    auto device = devices.begin()->second;


    if (device->colorCamera()) {
        device->colorCamera()->registerCallback( [](xv::ColorImage const & rgb){
            static FpsCount fc;
            fc.tic();
            static int k = 0;
            if(k++%25==0){
                std::cout << "rgb      " << timeShowStr(rgb.edgeTimestampUs, rgb.hostTimestamp)
                          << rgb.width << "x" << rgb.height << "@" << std::round(fc.fps()) << "fps" << std::endl;
            }
        });
        device->colorCamera()->setResolution(xv::ColorCamera::Resolution::RGB_1280x720);
        device->colorCamera()->start();
    } else {
        std::cout << "No RGB camera.\n";
    }

    if (device->tofCamera()) {

#ifdef USE_PRIVATE
        auto devPriv = std::dynamic_pointer_cast<xv::DevicePrivate>(device);
        devPriv->setTofIrEnabled(true);
        device->tofCamera()->registerCallback([](xv::DepthImage const & tof){
            if (tof.type == xv::DepthImage::Type::IR) {
                static FpsCount fc;
                fc.tic();
                static int k = 0;
                if(k++%15==0){
                    std::cout << "tof IR   " << timeShowStr(tof.edgeTimestampUs, tof.hostTimestamp)
                              << tof.width << "x" << tof.height << "@" << std::round(fc.fps()) << "fps" << std::endl;
                }
            }
        });
#endif

        device->tofCamera()->registerCallback([](xv::DepthImage const & tof){
            if (tof.type == xv::DepthImage::Type::Depth_16 || tof.type == xv::DepthImage::Type::Depth_32) {
                static FpsCount fc;
                fc.tic();
                static int k = 0;
                if(k++%15==0){
                    std::cout << "tof      " << timeShowStr(tof.edgeTimestampUs, tof.hostTimestamp)
                              << tof.width << "x" << tof.height << "@" << std::round(fc.fps()) << "fps" << std::endl;
                }
            }
        });
        device->tofCamera()->start();

        device->tofCamera()->registerColorDepthImageCallback([](const xv::DepthColorImage& depthColor){
            static FpsCount fc;
            fc.tic();
            static int k = 0;
            if(k++%15==0){
                std::cout << "RGBD     " << timeShowStr(depthColor.hostTimestamp)
                          << depthColor.width << "x" << depthColor.height << "@" << std::round(fc.fps()) << "fps" << std::endl;
            }
        });
    }

    if (device->imuSensor()) {
        device->imuSensor()->registerCallback([](xv::Imu const & imu){
            static FpsCount fc;
            fc.tic();
            static int k = 0;
            if(k++%500==0){
                std::cout << "imu      " << timeShowStr(imu.edgeTimestampUs, imu.hostTimestamp) << "@" << std::round(fc.fps()) << "fps" << " Accel(" << imu.accel[0] << "," << imu.accel[1] << "," << imu.accel[2] << "), Gyro(" << imu.gyro[0] << "," << imu.gyro[1] << "," << imu.gyro[2] << ")" << std::endl;
            }
        });
    }

    if (device->eventStream()) {
        device->eventStream()->registerCallback( [](xv::Event const & event){
            std::cout << "event      " << timeShowStr(event.edgeTimestampUs, event.hostTimestamp)
                      << " (" << event.type << "," << event.state << ")" << std::endl;
        });
        device->eventStream()->start();
    }

#ifdef USE_EX
    if (std::dynamic_pointer_cast<xv::DeviceEx>(device)->slam2()) {
        std::dynamic_pointer_cast<xv::DeviceEx>(device)->slam2()->registerCallback( [](const xv::Pose& pose){
            static FpsCount fc;
            fc.tic();
            static int k = 0;
            if(k++%500==0){
                auto pitchYawRoll = xv::rotationToPitchYawRoll(pose.rotation());
                std::cout << "edge-pose" << timeShowStr(pose.edgeTimestampUs(), pose.hostTimestamp()) << "@" << std::round(fc.fps()) << "fps" << " (" << pose.x() << "," << pose.y() << "," << pose.z() << ") (" << pitchYawRoll[0]*180/M_PI << "," << pitchYawRoll[1]*180/M_PI << "," << pitchYawRoll[2]*180/M_PI << ")" << pose.confidence() << std::endl;

            }
        });
        std::dynamic_pointer_cast<xv::DeviceEx>(device)->slam2()->start(xv::Slam::Mode::Edge);
    } else {
        std::cout << "No edge in camera.\n";
    }
#endif

    if (device->fisheyeCameras()) {
#ifdef USE_EX
        std::dynamic_pointer_cast<xv::FisheyeCamerasEx>(device->fisheyeCameras())->registerKeyPointsCallback([](const xv::FisheyeKeyPoints<2,32>& keypoints){
            static FpsCount fc;
            fc.tic();
            static int k = 0;
            if(k++%50==0){
                std::cout << "keypoints  "  << timeShowStr(keypoints.edgeTimestampUs, keypoints.hostTimestamp) << keypoints.descriptors[0].size << ":" << keypoints.descriptors[1].size << "@" << std::round(fc.fps()) << "fps" << std::endl;
            }
        });
#endif
        device->fisheyeCameras()->registerCallback([](xv::FisheyeImages const & stereo){
            static FpsCount fc;
            fc.tic();
            static int k = 0;
            if(k++%50==0){
                std::cout << "stereo   "  << timeShowStr(stereo.edgeTimestampUs, stereo.hostTimestamp) << stereo.images[0].width << "x" << stereo.images[0].height << "@" << std::round(fc.fps()) << "fps" << std::endl;
            }
        });
        device->fisheyeCameras()->start();
    }

    if (device->slam()) {
        device->slam()->registerCallback([](const xv::Pose& pose){
            static FpsCount fc;
            fc.tic();
            static int k = 0;
            if(k++%500==0){
                auto pitchYawRoll = xv::rotationToPitchYawRoll(pose.rotation());
                std::cout << "slam-pose" << timeShowStr(pose.edgeTimestampUs(), pose.hostTimestamp()) << "@" << std::round(fc.fps()) << "fps" << " (" << pose.x() << "," << pose.y() << "," << pose.z() << "," << pitchYawRoll[0]*180/M_PI << "," << pitchYawRoll[1]*180/M_PI << "," << pitchYawRoll[2]*180/M_PI << ")" << pose.confidence() << std::endl;
            }
        });

        device->slam()->registerStereoPlanesCallback([] (std::shared_ptr<const std::vector<xv::Plane>> planes) {
            if (!planes) return;
            static int k = 0;
            if(k++%30==0){
                std::cout << "Stereo-planes update (#" << planes->size() << " planes" << std::endl;
            }
        });

        device->slam()->start();
    }

#ifdef USE_EX
    // Must set device to edge mode to make both edge and mixed slam work.
    std::dynamic_pointer_cast<xv::DeviceEx>(device)->slam2()->start(xv::Slam::Mode::Edge);
#endif


    std::cout << " == Initialized ==" << std::endl;

#ifdef USE_OPENCV
    //Display in thread to not slow down callbacks

    if (device->colorCamera()) {
        device->colorCamera()->registerCallback( [](xv::ColorImage const & rgb){
        s_mtx_rgb.lock();
        s_rgb = std::make_shared<xv::ColorImage>(rgb);
        s_mtx_rgb.unlock();
        });
    }
    if (device->fisheyeCameras()) {
        device->fisheyeCameras()->registerCallback( [&device](xv::FisheyeImages const & stereo){
        s_mtx_stereo.lock();
        s_stereo = std::make_shared<xv::FisheyeImages>(stereo);
        s_mtx_stereo.unlock();
#ifdef USE_EX
        s_mtx_tags.lock();
        auto tags = std::dynamic_pointer_cast<xv::FisheyeCamerasEx>(device->fisheyeCameras())->detectTags(stereo.images[0], "36h11");
        s_tags = std::make_shared<std::vector<std::pair<int,std::array<xv::Vector2d,4>>>>(tags);
        s_mtx_tags.unlock();
#endif
        });
#ifdef USE_EX
        std::dynamic_pointer_cast<xv::FisheyeCamerasEx>(device->fisheyeCameras())->registerKeyPointsCallback([](const xv::FisheyeKeyPoints<2,32>& keypoints){
        s_mtx_stereo.lock();
        s_keypoints = std::make_shared<xv::FisheyeKeyPoints<2,32>>(keypoints);
        s_mtx_stereo.unlock();
        });
#endif
    }
    if (device->tofCamera()) {
        device->tofCamera()->registerCallback([](xv::DepthImage const & tof){
            if (tof.type == xv::DepthImage::Type::Depth_16 || tof.type == xv::DepthImage::Type::Depth_32) {
                std::lock_guard<std::mutex> l(s_mtx_tof);
                s_tof = std::make_shared<xv::DepthImage>(tof);
            } else if (tof.type ==  xv::DepthImage::Type::IR) {
                auto ir = std::make_shared<xv::GrayScaleImage>();
                ir->width = tof.width;
                ir->height = tof.height;
                ir->data = tof.data;
                std::lock_guard<std::mutex> l(s_mtx_ir);
                s_ir = ir;
            }
        });

        device->slam()->registerTofPlanesCallback([] (std::shared_ptr<const std::vector<xv::Plane>> planes) {
            static FpsCount fc;
            if (!planes) return;
            fc.tic();
            std::cout << "ToF-planes update (#" << planes->size() << " planes" << std::endl;
        });

        //std::dynamic_pointer_cast<xv::TofCameraEx>(device->tofCamera())->registerColorDepthImageCallback([](const xv::DepthColorImage& depthColor){
        device->tofCamera()->registerColorDepthImageCallback([](const xv::DepthColorImage& depthColor){
            s_mtx_depthColor.lock();
            s_depthColor = std::make_shared<xv::DepthColorImage>(depthColor);
            s_mtx_depthColor.unlock();
        });

    }

    s_stop = false;
    std::thread t(display);

#endif

    std::cout << " ################## " << std::endl;
    std::cout << "        Start       " << std::endl;
    std::cout << " ################## " << std::endl;

    std::cerr << "ENTER to stop" << std::endl;
    std::cin.get();

    s_stop = true;

    std::cout << " ################## " << std::endl;
    std::cout << "        Stop        " << std::endl;
    std::cout << " ################## " << std::endl;

#ifdef USE_EX
    if (std::dynamic_pointer_cast<xv::DeviceEx>(device)->slam2())
        std::dynamic_pointer_cast<xv::DeviceEx>(device)->slam2()->stop();
#endif

    if (device->slam())
        device->slam()->stop();


#ifdef USE_OPENCV
    s_stop = true;
    if (t.joinable()) {
        t.join();
    }
#endif

    return EXIT_SUCCESS;
}
catch( const std::exception &e){
    std::cerr << e.what() << std::endl;
    return EXIT_FAILURE;
}
