#include <opencv2/opencv.hpp>
#include <xv-sdk.h>

#include <cstring>
#include "colors.h"

cv::Mat raw_to_opencv(std::shared_ptr<const xv::ColorImage> rgb)
{
    cv::Mat img;
    switch (rgb->codec) {
    case xv::ColorImage::Codec::YUV420p: {
        img = cv::Mat::zeros(rgb->height, rgb->width, CV_8UC3);
        auto raw = rgb->data.get();
        auto rawImg = cv::Mat(rgb->height * 3 / 2, rgb->width, CV_8UC1, const_cast<unsigned char*>(raw));
        cv::cvtColor(rawImg, img, cv::COLOR_YUV2BGR_I420);
        break;
    }
    case xv::ColorImage::Codec::YUYV: {
        img = cv::Mat::zeros(rgb->height, rgb->width, CV_8UC3);
        auto raw = rgb->data.get();
        auto rawImg = cv::Mat(rgb->height, rgb->width, CV_8UC2, const_cast<unsigned char*>(raw));
        cv::cvtColor(rawImg, img, cv::COLOR_YUV2BGR_YUYV);
        break;
    }
    case xv::ColorImage::Codec::JPEG: {
        cv::Mat raw(1, rgb->width*rgb->height, CV_8UC1, const_cast<unsigned char*>(rgb->data.get()));
        img = cv::imdecode(raw, cv::IMREAD_COLOR);
        break;
    }
    }
    return img;
}

cv::Mat raw_to_opencv( std::shared_ptr<const xv::GrayScaleImage> tof_ir) {
    cv::Mat out;
    if( tof_ir ){
        out = cv::Mat::zeros(tof_ir->height, tof_ir->width, CV_8UC3);

        float dmax = 2191/4;

        auto tmp_d = reinterpret_cast<short const*>(tof_ir->data.get());

        for (unsigned int i=0; i< tof_ir->height*tof_ir->width; i++) {

            short d = tmp_d[i];

            unsigned int u = static_cast<unsigned int>( std::max(0.0f, std::min(255.0f,  d * 255.0f / dmax )));
            if( u < 15 )
                u = 0;
            const auto &cc = colors.at(u);
            out.at<cv::Vec3b>( i/tof_ir->width, i%tof_ir->width ) = cv::Vec3b(cc.at(2), cc.at(1),cc.at(0) );

            /*short u = tof->ir.get()[i];
                cvtof.at<cv::Vec3b>( i/tof->width, i%tof->width ) = cv::Vec3s(u*255/2191,u*255/2191,u*255/2191);*/
        }
    }
    return out;
}



cv::Mat raw_to_opencv( std::shared_ptr<const xv::DepthImage> tof)
{
    cv::Mat out;
    if (tof->height>0 && tof->width>0) {
        out = cv::Mat::zeros(tof->height, tof->width, CV_8UC3);
        if (tof->type == xv::DepthImage::Type::Depth_32) {
            float dmax = 7.5;
            const auto tmp_d = reinterpret_cast<float const*>(tof->data.get());
            for (unsigned int i=0; i< tof->height*tof->width; i++) {
                const auto &d = tmp_d[i];
                if( d < 0.01 || d > 9.9 ) {
                    out.at<cv::Vec3b>(i / tof->width, i % tof->width) = 0;
                } else {
                    unsigned int u = static_cast<unsigned int>( std::max(0.0f, std::min(255.0f,  d * 255.0f / dmax )));
                    const auto &cc = colors.at(u);
                    out.at<cv::Vec3b>( i/tof->width, i%tof->width ) = cv::Vec3b(cc.at(2), cc.at(1), cc.at(0) );
                }
            }
        } else if (tof->type == xv::DepthImage::Type::Depth_16) {
            // #define DEPTH_RANGE_20M_SF       7494  //7.494m
            // #define DEPTH_RANGE_60M_SF       2494  //2.494m
            // #define DEPTH_RANGE_100M_SF      1498  //1.498m
            // #define DEPTH_RANGE_120M_SF      1249  //1.249m
            float dmax = 2494.0; // maybe 7494,2494,1498,1249 see mode_manage.h in sony toflib
            const auto tmp_d = reinterpret_cast<int16_t const*>(tof->data.get());
            for (unsigned int i=0; i< tof->height*tof->width; i++) {
                const auto &d = tmp_d[i];
                unsigned int u = static_cast<unsigned int>( std::max(0.0f, std::min(255.0f,  d * 255.0f / dmax )));
                const auto &cc = colors.at(u);
                out.at<cv::Vec3b>( i/tof->width, i%tof->width ) = cv::Vec3b(cc.at(2), cc.at(1), cc.at(0) );
            }
        } else if( tof->type == xv::DepthImage::Type::IR ){
            out = cv::Mat::zeros(tof->height, tof->width, CV_8UC3);
            float dmax = 2494.0; // maybe 7494,2494,1498,1249 see mode_manage.h in sony toflib
            auto tmp_d = reinterpret_cast<unsigned short const*>(tof->data.get());
            for (unsigned int i=0; i< tof->height*tof->width; i++) {
                unsigned short d = tmp_d[i];
                unsigned int u = static_cast<unsigned int>( std::max(0.0f, std::min(255.0f,  d * 255.0f / dmax )));
                if( u < 15 )
                    u = 0;
                const auto &cc = colors.at(u);
                out.at<cv::Vec3b>( i/tof->width, i%tof->width ) = cv::Vec3b(cc.at(2), cc.at(1),cc.at(0) );
                /*short u = tof->ir.get()[i];
                cvtof.at<cv::Vec3b>( i/tof->width, i%tof->width ) = cv::Vec3s(u*255/2191,u*255/2191,u*255/2191);*/
            }
        }
    }
    return out;
}


std::pair<cv::Mat,cv::Mat> raw_to_opencv(std::shared_ptr<const xv::FisheyeImages> stereo)
{
    auto const& leftInput = stereo->images[0];
    auto const& rightInput = stereo->images[1];
    cv::Mat left = cv::Mat::zeros(leftInput.height, leftInput.width, CV_8UC1);
    cv::Mat right = cv::Mat::zeros(rightInput.height, rightInput.width, CV_8UC1);

    if( leftInput.data != nullptr ){
        std::memcpy(left.data, leftInput.data.get(), static_cast<size_t>(left.rows*left.cols));
    }
    if( rightInput.data != nullptr ){
        std::memcpy(right.data, rightInput.data.get(), static_cast<size_t>(right.rows*right.cols));
    }

    cv::cvtColor(left, left, cv::COLOR_GRAY2BGR);
    cv::cvtColor(right, right, cv::COLOR_GRAY2BGR);

    return {left, right};
}

cv::Mat raw_to_opencv( std::shared_ptr<const xv::DepthColorImage> rgbd)
{
    cv::Mat out;
    out = cv::Mat::zeros(rgbd->height, rgbd->width*2, CV_8UC3);
    auto w = rgbd->width;

    if (rgbd->height>0 && rgbd->width>0) {
        float dmax = 7.5;
        const auto tmp_d = reinterpret_cast<std::uint8_t const*>(rgbd->data.get()+3);
        for (unsigned int i=0; i< rgbd->height*rgbd->width; i++) {
            const auto &d = *reinterpret_cast<float const*>(tmp_d + i*(3+sizeof(float)));
            if( d < 0.01 || d > 9.9 ) {
                out.at<cv::Vec3b>(i / w, i % rgbd->width) = 0;
            } else {
                unsigned int u = static_cast<unsigned int>( std::max(0.0f, std::min(255.0f,  d * 255.0f / dmax )));
                const auto &cc = colors.at(u);
                out.at<cv::Vec3b>( i/ w, i%rgbd->width ) = cv::Vec3b(cc.at(2), cc.at(1), cc.at(0) );
            }
        }
        const auto tmp_rgb = reinterpret_cast<std::uint8_t const*>(rgbd->data.get());
        for (unsigned int i=0; i< rgbd->height*rgbd->width; i++) {
            const auto rgb = reinterpret_cast<std::uint8_t const*>(tmp_rgb + i*(3+sizeof(float)));
            out.at<cv::Vec3b>( i/ w, (i%rgbd->width) + rgbd->width) = cv::Vec3b(rgb[0], rgb[1], rgb[2]);
        }
    }
    return out;
}
