#pragma once

#include <map>
#include <functional>

#include "xv-types.h"

namespace xv {

class Device;


/**
 * @brief Stream interface.
 */
template <typename T>
class Stream {
public:

    using Data = T;

    /**
     * @brief start streaming.
     */
    virtual bool start() = 0;
    /**
     * @brief stop streaming.
     */
    virtual bool stop() = 0;

    /**
     * @brief Register callback to receive data.
     */
    virtual int registerCallback(std::function<void (T)>) = 0;
    /**
     * @brief Unregister callback.
     */
    virtual bool unregisterCallback(int callbackId) = 0;

};

/**
 * @brief Camera interface
 */
class Camera {
public:

    /**
     * @brief Get the camera calibration.
     *
     * The frames coordinates are defined according to the IMU frame coordinates. If 2 fisheyes cameras the first is left and second is right camera.
     */
    virtual const std::vector<Calibration>& calibration();

    // TODO add more settings like AWB in later release
    virtual bool setResolution( int resolution );
    virtual bool setFramerate( float framerate );

    // aecMode 0:auto 1:manual
    /**
     * @brief Exposure setting.
     * @param[in] aecMode 0:auto exposure 1:manual exposure
     * @param[in] exposureGain Only valid in manual exposure mode, [0,255]
     * @param[in] exposureTimeMs Only valid in manual exposure mode, in milliseconds
     */
    virtual bool setExposure( int aecMode = 0,  int exposureGain = 0,  float exposureTimeMs = 0.0 );

    /**
     * @brief Set output image brightness. Only valid in auto exposure mode
     * @param[in] brightness brightness of image, [0,255]
     */
    virtual bool setBrightness( int brightness );

    virtual ~Camera(){}
};

/**
 * @brief The class to give access to data provided by the IMU sensor.
 */
class ImuSensor : virtual public Stream<Imu const &> {
public:

    virtual ~ImuSensor(){}
};

/**
 * @brief A class to handle callbacks of events.
 */
class EventStream : virtual public Stream<Event const &> {
public:

    virtual ~EventStream(){}
};

/**
 * @brief The class to give access to 3dof data which converted from raw IMU data.
 */
class OrientationStream : virtual public Stream<Orientation const &> {
public:

    /**
     * @brief Get the current orientation of the device.
     *
     * The orientation (3dof) is the rotation of the IMU frame coordinates based on the world frame coordinates. The world frame coordinates coorespond the IMU coordinates when #startOrientation().
     *
     * @param[out] result orientation corresponding to the timestamp "now" + "prediction"
     * @param[in] prediction (in s) amount of prediction to use to get the orientation corresponding to the future
     * @return true if ok, false else.
     */
    virtual bool get(Orientation& pose, double prediction = 0.) = 0;

    /**
     * @brief Get the orientation of the device at a given timestamp.
     *
     * The orientation (3dof) is the rotation of the IMU frame coordinates based on the world frame coordinates. The world frame coordinates coorespond the IMU coordinates when #startOrientation().
     *
     * @param[out] result orientation corresponding to the timestamp, need to be not too in the pass or too in the future
     * @param[in] timestamp of the requested orientation, in s based on the host clock `std::chrono::steady_clock()`
     * @return true if the orientation can be returned, false else. If timestamp is too in the past or too in the future, return false.
     */
    virtual bool getAt(Orientation& pose, double timestamp) = 0;
    virtual ~OrientationStream(){}
};

/**
 * @brief The class to handle callbacks of the multi cameras for the visual SLAM.
 *
 * FisheyeCameras will get 2 or 4 #Calibration parameters. If 2 fisheyes cameras the first is left and second is right camera. For fisheye, per #Calibration only have one #UnifiedCameraModel and one #PolynomialDistortionCameraModel.
 */
class FisheyeCameras : virtual public Stream<FisheyeImages const &>, virtual public Camera {
public:

    virtual ~FisheyeCameras(){}
};

/**
 * @brief The class to handle informations about the display (if device can display like a HMD)
 */
class Display {
public:

    /**
     * @brief Get calibrations.
     *
     * Calibration parameters of the multiple parts of the dispaly (for HMD, first display is usually for left eye and second is for right eye).
     */
    virtual const std::vector<Calibration>& calibration() = 0;

    /**
     * @brief Turn on the display
     */
    virtual bool open() = 0;

    /**
     * @brief Turn off the display
     */
    virtual bool close() = 0;

    /**
     * @brief Set brightness level.
     *
     * @param[in] level display brightness level
     */
    virtual bool setBrightnessLevel( int level ) = 0;
};

/**
 * @brief A class to handle callbacks of the color image.
 */
class ColorCamera : virtual public Stream<ColorImage const &>, virtual public Camera {
public:
    enum class Resolution {
        RGB_1920x1080 = 0,  ///< RGB 1080p
        RGB_1280x720  = 1,  ///< RGB 720p
        RGB_640x480   = 2,  ///< RGB 480p
        RGB_320x240   = 3,  ///< RGB QVGA (not supported now)
        RGB_2560x1920 = 4,  ///< RGB 5m (not supported now)
    };

    virtual bool setResolution( const Resolution &resolution ) = 0;

    virtual ~ColorCamera(){}
};

/**
 * @brief A class to handle callbacks of the ToF camera
 */
class TofCamera : virtual public Stream<DepthImage const &>, virtual public Camera {
public:
    enum class StreamMode { DepthOnly = 0, CloudOnly, DepthAndCloud, None };
    enum class DistanceMode { Short = 0, Middle, Long };

    /**
     * @brief Gives access to composed image with RBG color on depth images.
     */
    virtual int registerColorDepthImageCallback(std::function<void(const DepthColorImage&)>) = 0;
    virtual bool unregisterColorDepthImage(int callbackId) = 0;

    /**
     * @brief Convert a depth image to point cloud.
     * @return  The point cloud of valid depth image pixels in ToF frame coordinates, nullptr if something went wrong.
     */
    virtual std::shared_ptr<PointCloud> depthImageToPointCloud(DepthImage const &) const = 0;

    /**
     * @brief Set which stream will be reported. Not work with sony TOF.
     */
    virtual bool setStreamMode(StreamMode mode) = 0;

    /**
     * @brief Set distance mode.
     *
     * Midlle=Short for 010/009 TOF.
     */
    virtual bool setDistanceMode(DistanceMode mode) = 0;

    /**
     * @brief Set work mode.
     */
    virtual bool setMode(int mode) = 0;

    virtual ~TofCamera() {}
};

/**
 * @brief The class to represent the component doing the 6dof tracking with SLAM algorithm on host.
 *
 * For Mixed mode, callback will get the last computed SLAM pose. Callback is call on each IMU recieved because SLAM pose also use IMU for update.
 */
class Slam : virtual public Stream<Pose const&> {

public:
    enum class Mode { Edge = 0, Mixed, EdgeFusionOnHost };

    /**
     * @brief Get #Mode
     * @return return slam mode of this object, mode is const.
     */
    virtual Mode mode() const = 0;

    /**
     * @brief Start slam of current mode.
     */
    virtual bool start() override = 0;

    /**
     * @brief Start slam of specific mode.
     *
     * Stop old mode slam(if running), switch to new mode, and then start slam of new mode.
     */
    virtual bool start(Mode mode) = 0;

    /**
     * @brief Reset the 6dof tracker (SLAM)
     * @return return true if well reset, else if something went wrong.
     */
    virtual bool reset() = 0;

    /**
     * @brief Get the current 6dof pose of the device.
     *
     * The 6dof pose is the rotation and translation of the IMU frame coordinates based on the world frame coordinates. The world frame coordinates coorespond to the VLSAM map and the device
     * coordinates is based on the IMU coordinates.
     *
     * @param[out] pose corresponding to the timestamp "now" + "prediction"
     * @param[in] prediction (in s) amount of prediction to use to get a pose corresponding to the future
     * @return true if ok, false else.
     */
    virtual bool getPose(Pose& pose, double prediction = 0.) = 0;

    /**
     * @brief Get the 6dof pose of the device at a given timestamp.
     *
     * The 6dof pose is the rotation and translation of the IMU frame coordinates based on the world frame coordinates. The world frame coordinates coorespond to the VLSAM map and the device
     * coordinates is based on the IMU coordinates.
     *
     * @param[out] pose result pose corresponding to the timestamp, need to be not too in the pass or too in the future
     * @param[in] timestamp of the wanted pose, in s based on the host clock `std::chrono::steady_clock()`
     * @return true if the pose can be returned, false else. If timestamp is too in the past or too in the future, return false.
     */
    virtual bool getPoseAt(Pose& pose, double timestamp) = 0;

    /**
     * @brief Register a callback called when SLAM is lost.
     * @return Id of the callback (used to unregister the callback).
     */
    virtual int registerLostCallback(std::function<void ()> lostCallback) = 0;
    virtual bool unregisterLostCallback(int callbackId) = 0;

    /**
     * @brief Callback to get the detected planes using stereo cameras and SLAM
     *
     * The vector contains planes with current planes and each plane has an ID. Between mutiple calls new planes can be added, previous planes updated or merged. If a plane disappear from the vector, it means it was merged with other.
     *
     * @return Id of the callback (used to unregister the callback).
     */
    virtual int registerStereoPlanesCallback(std::function<void (std::shared_ptr<const std::vector<Plane>>)> planeCallback) = 0;
    virtual bool unregisterStereoPlanesCallback(int callbackId) = 0;
    virtual bool clearStereoPlanes() = 0;

    /**
     * @brief Callback to get the detected planes using ToF camera and SLAM
     *
     * The vector contains planes with current planes and with plane ID as key. Between mutiple calls new planes can be added, previous planes updated or merged. If a plane disappear from the vector, it means it was merged with other.
     *
     * @return Id of the callback (used to unregister the callback).
     */
    virtual int registerTofPlanesCallback(std::function<void (std::shared_ptr<const std::vector<Plane>>)> planeCallback) = 0;
    virtual bool unregisterTofPlanesCallback(int callbackId) = 0;
    virtual bool clearTofPlanes() = 0;


    /**
     * @brief Callback to get the SLAM map updates.
     * @return Id of the callback (used to unregister the callback).
     */
    virtual int registerMapCallback(std::function<void (std::shared_ptr<const xv::SlamMap>)> mapCallback) = 0;
    virtual bool unregisterMapCallback(int callbackId) = 0;

    /**
     * @brief Load a SLAM map and use it as an immutable reference map.
     *
     * @param mapStream the input map stream for loading the map
     * @param done_callback When the switch is done the callback will be called. The input of the callback is the quality result (0-100) of the reference map.
     * @param localized_on_reference_map Call the callback if the SLAM uses a reference map and is localized on the reference map. The input parameter is the threshold for the percentage [0.f,1.f]
     * of reference map usage according to whole map (reference map and dynamic map).
     * If SLAM is not currently using enough the reference map and the usage is below this threshold, then the callback is called with the current percentage [0.f,1.f] of
     * 3D points from the reference map.
     */
    virtual bool loadMapAndSwitchToCslam(std::streambuf& mapStream, std::function<void(int /* status of load map */)> done_callback, std::function<void(float)> localized_on_reference_map={}) = 0;
    /**
     * @brief Save a SLAM map and use it as an immutable reference map.
     *
     * @param mapStream the output map stream to for writing the map
     * @param done_callback When the switch is done the callback will be called. The input of the callback is the quality result (0-100) of the reference map.
     * @param localized_on_reference_map Call the callback if the SLAM uses a reference map and is localized on the reference map. The input parameter is the threshold for the percentage [0.f,1.f]
     * of reference map usage according to whole map (reference map and dynamic map).
     * If SLAM is not currently using enough the reference map and the usage is below this threshold, then the callback is called with the current percentage [0.f,1.f] of
     * 3D points from the reference map.
     */
    virtual bool saveMapAndSwitchToCslam(std::streambuf& mapStream, std::function<void(int /* status of save map */, int /* map quality */)> done_callback, std::function<void(float)> localized_on_reference_map={}) = 0;

    virtual ~Slam() {}
};

/**
 * @brief A class to handle callbacks of the object detector (CNN)
 */
class ObjectDetector : virtual public Stream<std::vector<Object> const&> {
public:
    enum class Source { LEFT = 0, RIGHT, RGB, TOF };

    virtual bool setDescriptor( const std::string &filepath ) = 0;
    virtual bool setModel( const std::string &filepath ) = 0;
    virtual bool setSource( const Source &source ) = 0;
    virtual xv::ObjectDetector::Source getSource() const = 0;
    virtual xv::ObjectDescriptor getDescriptor() const = 0;

    virtual ~ObjectDetector() {}
};

/**
 * @brief A class to handle callbacks of the SGBM.
 */
class SgbmCamera : virtual public Stream<SgbmImage const &>, virtual public Camera {
public:

    enum class Mode { Hardware = 0, Software };
    /**
     * @brief Must be called befor start.
     */
    virtual Mode mode() const = 0;

    virtual bool start(const std::string &sgbmConfig) = 0;
    virtual bool setConfig(const std::string &sgbmConfig) = 0;
    virtual ~SgbmCamera() {}
};

/**
 * @brief A class to handle callbacks of the thermal camera.
 */
class ThermalCamera : virtual public Stream<ThermalImage const &>, virtual public Camera {
public:

    enum class Mode { TEMPRETURE = 0, GREY };
    virtual bool setMode( Mode mode ) = 0;
    virtual ~ThermalCamera() {}
};

/**
 * @brief A class to handle callbacks of the eyetracking camera.
 */
class EyetrackingCamera : virtual public Stream<EyetrackingImage const &>, virtual public Camera {
public:

    /**
     * @brief Set eyetracking exposure
     *
     * @param[in] leftGain Left eye exposure gain, [0, 255]
     * @param[in] leftTimeMs Left eye exposure time, in milliseconds
     * @param[in] rightGain Right eye exposure gain, [0, 255]
     * @param[in] rightTimeMs Right eye exposure time, in milliseconds
     */
    virtual bool setExposure( int leftGain, float leftTimeMs, int rightGain, float rightTimeMs ) = 0;

    /**
     * @brief Set eyetracking led brightness (in s)
     *
     * @param[in] eye 0:left, 1:right, 2:both
     * @param[in] led [0,7]:led index, 8:all
     * @param[in] brightness [0,255], 0 is off
     */
    virtual bool setLedBrighness( int eye, int led, int brightness ) = 0;

    virtual ~EyetrackingCamera() {}
};

/**
 * @brief A class to handle MIC. Adjust volumn through source.
 */
class MicStream : virtual public Stream<MicData const &> {
public:

    virtual ~MicStream() {}
};

/**
 * @brief A class to handle speaker. Adjust the sound source(PCM) volume to adjust the volume.
 */
class Speaker {
public:

    virtual bool enable() = 0;
    virtual bool disable() = 0;
    /**
     * @brief Send a small time slice of sound data.
     */
    virtual int  send(const std::uint8_t *data, int len) = 0;

    /**
     * @brief Async play sound file in new thread.
     */
    virtual bool play(const std::string &path) = 0;
    /**
     * @brief Async play buffer in new thread.
     */
    virtual bool play(const std::uint8_t *data, int len) = 0;
    /**
     * @brief If async playing.
     */
    virtual bool isPlaying() = 0;
    /**
     * @brief Rigster a callback for async play end.
     */
    virtual int registerPlayEndCallback( std::function<void ()> ) = 0;
    virtual bool unregisterPlayEndCallback( int callbackId ) = 0;

    virtual ~Speaker() {}
};

/**
 * @brief Class to get tracking results and raw outputs with a connected device.
 *
 * This class is the main entry point of the API, it gives access to the device and algorithms. See xv::getDevices() to have an instance corresponding to a device.
 *
 * A device can have multiple components, accessible with member functions :
 * - #xv::Device::slam() : 6dof tracker doing the SLAM algorithm on host based on informations coming from device (stereo camera, IMU sensor, ..)
 * - #xv::Device::imuSensor() : sensor with at least 3-axis accelerometer and 3-axis gyrometer
 * - #xv::Device::fisheyeCameras(): typically 2 fisheye cameras used for Visual SLAM
 * - #xv::Device::tofCamera(): a depth camera sensor
 * - #xv::Device::edge(): used to run some algorithm directly on embedded device (typically Visual SLAM) when it is possible to choose between host and edge processing
 * - #xv::Device::display(): used to handle informations about the display (if device can display like a HMD)
 * - #xv::Device::objectDetector(): used to run and get results of the CNN object detector
 *
 * If a device does not support a component or doesn't have the component (for example ToF), the accessor function will return `null_ptr`.
 * The data streams and processings under a component are activated only if at least one callback is registerd to the component. If all the callbacks are unregistered then steams can be deactivated.
 */
class Device {

public:


    /**
     * @brief Get informations (Serial Number, version ...) about the device.
     * @return A map with key and values of the informations.
     */
    virtual std::map<std::string, std::string> info() = 0;

    /**
     * @brief Get the SLAM component.
     */
    virtual std::shared_ptr<Slam> slam() = 0;

    /**
     * @brief Get the IMU sensor of the device.
     */
    virtual std::shared_ptr<ImuSensor> imuSensor() = 0;

    /**
     * @brief Get the event component.
     */
    virtual std::shared_ptr<EventStream> eventStream() = 0;

    /**
     * @brief Get the 3dof component.
     */
    virtual std::shared_ptr<OrientationStream> orientationStream() = 0;

    /**
     * @brief Get the stereo cameras component of the device.
     */
    virtual std::shared_ptr<FisheyeCameras> fisheyeCameras() = 0;

    /**
     * @brief Get the color camera component of the device.
     */
    virtual std::shared_ptr<ColorCamera> colorCamera() = 0;

    /**
     * @brief Get the ToF component of the device.
     */
    virtual std::shared_ptr<TofCamera> tofCamera() = 0;

    /**
     * @brief Get the SGBM component of the device.
     */
    virtual std::shared_ptr<SgbmCamera> sgbmCamera() = 0;

    /**
     * @brief Get the thermal component of the device.
     */
    virtual std::shared_ptr<ThermalCamera> thermalCamera() = 0;

    /**
     * @brief Get the eyetracking component of the device.
     */
    virtual std::shared_ptr<EyetrackingCamera> eyetracking() = 0;

    /**
     * @brief Get the MIC component of the device.
     */
    virtual std::shared_ptr<MicStream> mic() = 0;

    /**
     * @brief Get the speaker component of the device.
     */
    virtual std::shared_ptr<Speaker> speaker() = 0;

    /**
     * @brief Get the display component.
     */
    virtual std::shared_ptr<Display> display() = 0;

    /**
     * @brief Get the object detection component.
     */
    virtual std::shared_ptr<ObjectDetector> objectDetector() = 0;

    /**
     * @brief Let device sleep.
     */
    virtual bool sleep(int level = 0) = 0;
    /**
     * @brief Wake up device.
     */
    virtual bool wakeup() = 0;

    /**
     * @brief Control device.
     */
    virtual bool control(const DeviceSetting &setting) = 0;

    /**
     * @brief Write HID control command and read result.
     */
    virtual bool hidWriteAndRead(const std::vector<unsigned char> &command, std::vector<unsigned char> &result) = 0;
    /**
     * @brief Write UVC control command and read result.
     */
    virtual bool uvcWriteAndRead(const std::vector<unsigned char> &command, std::vector<unsigned char> &result) = 0;
    /**
     * @brief Write VSC control command and read result.
     */
    virtual bool vscWriteAndRead(const std::vector<unsigned char> &command, std::vector<unsigned char> &result) = 0;

    /**
     * @brief Return the serial number of the device.
     */
    virtual std::string id() const = 0;

    virtual ~Device(){}

};


/**
 * \defgroup xv_functions Global functions
 * @{
 */

/**
 * @brief Get xvsdk version.
 */
Version version();

/**
 * @brief Retrieve all the detected XVisio devices.
 * If no device is found after the timeout is reached, the result will be empty.
 * @param timeOut : wait until the timeout is reached or find at least one device.
 * @param stopWaiting : stop scanning when become true.
 * @param desc : Load device according to feature in desc(json string). SDK can choose device feature from desc accoring SN or hardware version. The desc also contains default values of slam algorithm(old SDK is INI file).
 * @return A map with key corresponding to device ID and the value is a #Device.
 */
std::map<std::string,std::shared_ptr<Device>> getDevices(double timeOut = 0., const std::string& desc = "", bool* stopWaiting = nullptr);

/**
 * @brief Change the log level.
 */
void setLogLevel(LogLevel l);

/**
 * @brief Register the callback for hotplug.
 * @return Id of the callback (used to unregister the callback).
 */
int registerPlugEventCallback(const std::function<void (std::shared_ptr<Device> device, PlugEventType type)> &Callback);
/**
 * @brief Unregister a plug callback.
 */
bool unregisterHotplugCallback( int callbackID );

/**
 * @}
 */


/**
 * \defgroup xv_android_functions Functions for Android
 * @{
 */
/**
 * @brief Retrieve #Device by given descriptor. Only for Android.
 * @param fd : file descriptor opened by android USBManager.
 * @return A #Device.
 */
std::shared_ptr<Device> getDevice(int fd);
/**
 * @brief Retrieve #Device by given descriptor. Only for Android.
 * @param fd : file descriptor opened by android USBManager.
 * @param desc : load device according to feature in desc. SDK can choose device feature from desc accoring SN or version.
 * @return A #Device.
 */
std::shared_ptr<Device> getDevice(int fd, std::string const& desc);

/**
 * @brief Tell sdk device has connected. Only for Android.
 */
bool attach( int fd );
/**
 * @brief Tell sdk device has disconnected. Only for Android.
 */
bool detach(int fd );

/**
 * @}
 */

}
