var searchData=
[
  ['patch_75',['patch',['../structxv_1_1Version.html#aa7e14daf6de6cbd6f0c77bf63da30877',1,'xv::Version']]],
  ['pdcm_76',['pdcm',['../structxv_1_1Calibration.html#a750e9d4701caa6c0e240c342b67698e8',1,'xv::Calibration']]],
  ['pdmcameracalibration_77',['PdmCameraCalibration',['../structxv_1_1PdmCameraCalibration.html',1,'xv']]],
  ['plane_78',['Plane',['../structxv_1_1Plane.html',1,'xv']]],
  ['play_79',['play',['../classxv_1_1Speaker.html#a02f777b74ff78b579d7f203165446e37',1,'xv::Speaker::play(const std::string &amp;path)=0'],['../classxv_1_1Speaker.html#a0f199d2d4e265876d1e04149892079b2',1,'xv::Speaker::play(const std::uint8_t *data, int len)=0']]],
  ['pointcloud_80',['PointCloud',['../structxv_1_1PointCloud.html',1,'xv']]],
  ['points_81',['points',['../structxv_1_1Plane.html#ae65f776acd4a32ddd65c8ae5c8d01832',1,'xv::Plane']]],
  ['polynomialdistortioncameramodel_82',['PolynomialDistortionCameraModel',['../structxv_1_1PolynomialDistortionCameraModel.html',1,'xv']]],
  ['pose_83',['Pose',['../structxv_1_1Pose.html',1,'xv::Pose'],['../structxv_1_1Pose.html#a601a7c9f28b654277602d7e0e0d570ce',1,'xv::Pose::Pose()']]],
  ['pose_5f_84',['Pose_',['../classxv_1_1details_1_1Pose__.html',1,'xv::details::Pose_&lt; F &gt;'],['../classxv_1_1details_1_1Pose__.html#abbc3060b2c0c2609ff6e59c6ba0a18ca',1,'xv::details::Pose_::Pose_(double c)'],['../classxv_1_1details_1_1Pose__.html#a58515cec49184f6d0d1ff39f2e5c7b97',1,'xv::details::Pose_::Pose_(Vector3&lt; F &gt; const &amp;translation, Matrix3&lt; F &gt; const &amp;rotation, double hostTimestamp=std::numeric_limits&lt; double &gt;::infinity(), std::int64_t edgeTimestamp=(std::numeric_limits&lt; std::int64_t &gt;::min)(), double c=0.)']]],
  ['pose_5f_3c_20double_20_3e_85',['Pose_&lt; double &gt;',['../classxv_1_1details_1_1Pose__.html',1,'xv::details']]],
  ['pose_5f_3c_20float_20_3e_86',['Pose_&lt; float &gt;',['../classxv_1_1details_1_1Pose__.html',1,'xv::details']]],
  ['posef_87',['PoseF',['../structxv_1_1PoseF.html',1,'xv::PoseF'],['../structxv_1_1PoseF.html#aca9217df44397806017c8c5d25347df9',1,'xv::PoseF::PoseF()']]],
  ['posepred_5f_88',['PosePred_',['../classxv_1_1details_1_1PosePred__.html',1,'xv::details::PosePred_&lt; F &gt;'],['../classxv_1_1details_1_1PosePred__.html#ab2ae33ec9feb62b33fd5ca55c40abe72',1,'xv::details::PosePred_::PosePred_(double c)'],['../classxv_1_1details_1_1PosePred__.html#a8b67f20e97e8764b847e7ee78792e3f0',1,'xv::details::PosePred_::PosePred_(Vector3&lt; F &gt; const &amp;translation, Matrix3&lt; F &gt; const &amp;rotation, double hostTimestamp=std::numeric_limits&lt; double &gt;::infinity(), std::int64_t edgeTimestamp=(std::numeric_limits&lt; std::int64_t &gt;::min)(), double c=0.)']]],
  ['posepred_5f_3c_20double_20_3e_89',['PosePred_&lt; double &gt;',['../classxv_1_1details_1_1PosePred__.html',1,'xv::details']]],
  ['posepred_5f_3c_20float_20_3e_90',['PosePred_&lt; float &gt;',['../classxv_1_1details_1_1PosePred__.html',1,'xv::details']]],
  ['posequat_5f_91',['PoseQuat_',['../classxv_1_1details_1_1PoseQuat__.html',1,'xv::details']]],
  ['poserot_5f_92',['PoseRot_',['../classxv_1_1details_1_1PoseRot__.html',1,'xv::details']]],
  ['poserot_5f_3c_20double_20_3e_93',['PoseRot_&lt; double &gt;',['../classxv_1_1details_1_1PoseRot__.html',1,'xv::details']]],
  ['poserot_5f_3c_20float_20_3e_94',['PoseRot_&lt; float &gt;',['../classxv_1_1details_1_1PoseRot__.html',1,'xv::details']]],
  ['prediction_95',['prediction',['../structxv_1_1Pose.html#a70390e0800990e077cba77a5bc209a07',1,'xv::Pose::prediction()'],['../structxv_1_1PoseF.html#ad1bda0124ef02fc362a4bc75223f4473',1,'xv::PoseF::prediction()'],['../classxv_1_1Orientation.html#a9c7f9ef7e4cc1fad77e8b6d4a13cb63e',1,'xv::Orientation::prediction()']]]
];
