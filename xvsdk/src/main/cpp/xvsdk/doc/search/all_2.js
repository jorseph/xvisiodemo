var searchData=
[
  ['d_15',['d',['../structxv_1_1Plane.html#a3f4f5ab5ddc9a6a739260f6364066b60',1,'xv::Plane']]],
  ['datasize_16',['dataSize',['../structxv_1_1ColorImage.html#a928a65aa21303a71973180ef4f40166d',1,'xv::ColorImage::dataSize()'],['../structxv_1_1DepthImage.html#adbf08bc641746ca1bb9e1aa380e23951',1,'xv::DepthImage::dataSize()'],['../structxv_1_1SgbmImage.html#a048d70950d25cbca78d86dc75b8449a8',1,'xv::SgbmImage::dataSize()'],['../structxv_1_1ThermalImage.html#a6e62ed9656004c9e95da98bc62f3c1c7',1,'xv::ThermalImage::dataSize()'],['../structxv_1_1MicData.html#af579d1c45a023399593af6b6ee62b2c7',1,'xv::MicData::dataSize()']]],
  ['depthcolorimage_17',['DepthColorImage',['../structxv_1_1DepthColorImage.html',1,'xv']]],
  ['depthimage_18',['DepthImage',['../structxv_1_1DepthImage.html',1,'xv']]],
  ['depthimagetopointcloud_19',['depthImageToPointCloud',['../classxv_1_1TofCamera.html#a437c5475eed22400f867802b526ac825',1,'xv::TofCamera']]],
  ['detach_20',['detach',['../group__xv__android__functions.html#ga86d796eefcf58185b4e0dd35865928d6',1,'xv']]],
  ['device_21',['Device',['../classxv_1_1Device.html',1,'xv']]],
  ['devicesetting_22',['DeviceSetting',['../structxv_1_1DeviceSetting.html',1,'xv']]],
  ['display_23',['Display',['../classxv_1_1Display.html',1,'xv::Display'],['../classxv_1_1Device.html#a088d0774607e803076cb889e70af2036',1,'xv::Device::display()']]],
  ['distor_24',['distor',['../structxv_1_1PolynomialDistortionCameraModel.html#ac2e38172020b36ba3011dc13e98f0653',1,'xv::PolynomialDistortionCameraModel']]]
];
