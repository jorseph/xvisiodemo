var searchData=
[
  ['objectdetector_312',['objectDetector',['../classxv_1_1Device.html#a16049ac3cef9f3035e45675a63dfaa8d',1,'xv::Device']]],
  ['open_313',['open',['../classxv_1_1Display.html#aab75c01236ff39c0a0795b8784680da2',1,'xv::Display']]],
  ['operator_2a_3d_314',['operator*=',['../classxv_1_1details_1_1Transform__.html#a45d863d291f36f7ec7efdb8e10ce57b3',1,'xv::details::Transform_']]],
  ['orientation_315',['Orientation',['../classxv_1_1Orientation.html#ab71a8cabbe72bf74aace21e72c239850',1,'xv::Orientation::Orientation(Matrix3d const &amp;rotation, double hostTimestamp=std::numeric_limits&lt; double &gt;::infinity(), std::int64_t edgeTimestamp=(std::numeric_limits&lt; std::int64_t &gt;::min)())'],['../classxv_1_1Orientation.html#a130b978a00f0c769bbada603ea654cd3',1,'xv::Orientation::Orientation(Vector4d const &amp;quaternion, double hostTimestamp=std::numeric_limits&lt; double &gt;::infinity(), std::int64_t edgeTimestamp=(std::numeric_limits&lt; std::int64_t &gt;::min)())']]],
  ['orientationstream_316',['orientationStream',['../classxv_1_1Device.html#aacbaf155af887f81356a4b02d1b9df06',1,'xv::Device']]]
];
