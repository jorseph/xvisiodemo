var searchData=
[
  ['calibration_8',['Calibration',['../structxv_1_1Calibration.html',1,'xv::Calibration'],['../classxv_1_1Camera.html#a100df43716a6cf8bd733ca6aa669d322',1,'xv::Camera::calibration()'],['../classxv_1_1Display.html#a54bac3fec8232fc5712736ef03ef8a11',1,'xv::Display::calibration()']]],
  ['camera_9',['Camera',['../classxv_1_1Camera.html',1,'xv']]],
  ['close_10',['close',['../classxv_1_1Display.html#a3d2c089e316d6738ad0b99374028c9f2',1,'xv::Display']]],
  ['colorcamera_11',['ColorCamera',['../classxv_1_1ColorCamera.html',1,'xv::ColorCamera'],['../classxv_1_1Device.html#af29a558592c5f4e4bda549e40afd981f',1,'xv::Device::colorCamera()']]],
  ['colorimage_12',['ColorImage',['../structxv_1_1ColorImage.html',1,'xv']]],
  ['confidence_13',['confidence',['../structxv_1_1DepthImage.html#a391c9588682071fc5b9c93d4f5581fc7',1,'xv::DepthImage::confidence()'],['../classxv_1_1details_1_1Pose__.html#a670ef08a0c91a82bfd64931469d305ad',1,'xv::details::Pose_::confidence()']]],
  ['control_14',['control',['../classxv_1_1Device.html#a31189faad2b365ae580697ad5614c47f',1,'xv::Device']]]
];
