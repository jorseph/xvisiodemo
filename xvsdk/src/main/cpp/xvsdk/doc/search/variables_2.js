var searchData=
[
  ['d_385',['d',['../structxv_1_1Plane.html#a3f4f5ab5ddc9a6a739260f6364066b60',1,'xv::Plane']]],
  ['datasize_386',['dataSize',['../structxv_1_1ColorImage.html#a928a65aa21303a71973180ef4f40166d',1,'xv::ColorImage::dataSize()'],['../structxv_1_1DepthImage.html#adbf08bc641746ca1bb9e1aa380e23951',1,'xv::DepthImage::dataSize()'],['../structxv_1_1SgbmImage.html#a048d70950d25cbca78d86dc75b8449a8',1,'xv::SgbmImage::dataSize()'],['../structxv_1_1ThermalImage.html#a6e62ed9656004c9e95da98bc62f3c1c7',1,'xv::ThermalImage::dataSize()'],['../structxv_1_1MicData.html#af579d1c45a023399593af6b6ee62b2c7',1,'xv::MicData::dataSize()']]],
  ['distor_387',['distor',['../structxv_1_1PolynomialDistortionCameraModel.html#ac2e38172020b36ba3011dc13e98f0653',1,'xv::PolynomialDistortionCameraModel']]]
];
