var indexSectionsWithContent =
{
  0: "acdefghiklmnopqrstuvwxyz",
  1: "acdefgikmoprstuv",
  2: "x",
  3: "acdefghilmopqrstuvwxyz",
  4: "acdefghimnpstuvwx",
  5: "r",
  6: "r",
  7: "afgv"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues",
  7: "groups"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Variables",
  5: "Enumerations",
  6: "Enumerator",
  7: "Modules"
};

