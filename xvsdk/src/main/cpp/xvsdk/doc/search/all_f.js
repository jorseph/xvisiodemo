var searchData=
[
  ['registercallback_103',['registerCallback',['../classxv_1_1Stream.html#a77065069930031e9c1397d8d2af11a4b',1,'xv::Stream']]],
  ['registercolordepthimagecallback_104',['registerColorDepthImageCallback',['../classxv_1_1TofCamera.html#a5ffb7904edee69e6894895e655a76f14',1,'xv::TofCamera']]],
  ['registerlostcallback_105',['registerLostCallback',['../classxv_1_1Slam.html#adcd3a606402c9b6e2efcef0319c8b93f',1,'xv::Slam']]],
  ['registermapcallback_106',['registerMapCallback',['../classxv_1_1Slam.html#a2e482f687e0970dfa1c9abad5278e485',1,'xv::Slam']]],
  ['registerplayendcallback_107',['registerPlayEndCallback',['../classxv_1_1Speaker.html#a350a747a843a649f62e53f8a4f9cdc30',1,'xv::Speaker']]],
  ['registerplugeventcallback_108',['registerPlugEventCallback',['../group__xv__functions.html#ga7981b8ae235923b9f462a21c0c3b8306',1,'xv']]],
  ['registerstereoplanescallback_109',['registerStereoPlanesCallback',['../classxv_1_1Slam.html#a197e1046b163085cdf86c5e0d1d07c79',1,'xv::Slam']]],
  ['registertofplanescallback_110',['registerTofPlanesCallback',['../classxv_1_1Slam.html#a4d9b5d29be7e8f952ba3e6501ab817a2',1,'xv::Slam']]],
  ['reset_111',['reset',['../classxv_1_1Slam.html#afe502b3d0fd50405a7528659124bce75',1,'xv::Slam']]],
  ['resolution_112',['Resolution',['../classxv_1_1ColorCamera.html#acb488c507a1956ac0016953a60740cd4',1,'xv::ColorCamera']]],
  ['rgb_5f1280x720_113',['RGB_1280x720',['../classxv_1_1ColorCamera.html#acb488c507a1956ac0016953a60740cd4a0204b6588463932d4cffb3d491f995cf',1,'xv::ColorCamera']]],
  ['rgb_5f1920x1080_114',['RGB_1920x1080',['../classxv_1_1ColorCamera.html#acb488c507a1956ac0016953a60740cd4ad443f2678079887c727958de510a1bf3',1,'xv::ColorCamera']]],
  ['rgb_5f2560x1920_115',['RGB_2560x1920',['../classxv_1_1ColorCamera.html#acb488c507a1956ac0016953a60740cd4af3c688ba31f9bc87d6aeb7de15aa7d5c',1,'xv::ColorCamera']]],
  ['rgb_5f320x240_116',['RGB_320x240',['../classxv_1_1ColorCamera.html#acb488c507a1956ac0016953a60740cd4ab2033a0bd383b22f20c9341003743f75',1,'xv::ColorCamera']]],
  ['rgb_5f640x480_117',['RGB_640x480',['../classxv_1_1ColorCamera.html#acb488c507a1956ac0016953a60740cd4a8d7edbd65d6c73ca311635fe0bf6a3f0',1,'xv::ColorCamera']]],
  ['rgbimage_118',['RgbImage',['../structxv_1_1RgbImage.html',1,'xv::RgbImage'],['../structxv_1_1RgbImage.html#a600cb5486d8dc1d6bb09d3d8c7b41439',1,'xv::RgbImage::RgbImage()']]],
  ['rotation_119',['rotation',['../classxv_1_1details_1_1Transform__.html#ade74f0f888c9c3727e737114d39689f4',1,'xv::details::Transform_::rotation()'],['../classxv_1_1Orientation.html#a2956c450561320ae98ae00c796da298f',1,'xv::Orientation::rotation()']]],
  ['rotationtopitchyawroll_120',['rotationToPitchYawRoll',['../group__xv__rotation__conversions.html#ga2e933d4631944d4ab6ff3d0aa0ce7223',1,'xv::rotationToPitchYawRoll(Matrix3d const &amp;rot)'],['../group__xv__rotation__conversions.html#gad1ab736f853d0e899de83452d916adb3',1,'xv::rotationToPitchYawRoll(Matrix3f const &amp;rot)']]],
  ['rotationtoquaternion_121',['rotationToQuaternion',['../group__xv__rotation__conversions.html#gaee4a8cad3cd0fd3ffa551bc85c6ab466',1,'xv::rotationToQuaternion(Matrix3f const &amp;rot)'],['../group__xv__rotation__conversions.html#ga65fcec2c0e1d5f7eb5180841d0845782',1,'xv::rotationToQuaternion(Matrix3d const &amp;rot)']]]
];
