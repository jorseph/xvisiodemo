var searchData=
[
  ['get_295',['get',['../classxv_1_1OrientationStream.html#aad74ada8cf221d75d226fbcb1be671e3',1,'xv::OrientationStream']]],
  ['getat_296',['getAt',['../classxv_1_1OrientationStream.html#a4d0d580225445a93bd16bbdada338f24',1,'xv::OrientationStream']]],
  ['getdevice_297',['getDevice',['../group__xv__android__functions.html#gaef8a3bcde35be6aa07323e4f329f2d98',1,'xv::getDevice(int fd)'],['../group__xv__android__functions.html#ga5677c746c6ad376e8533ec10666b210a',1,'xv::getDevice(int fd, std::string const &amp;desc)']]],
  ['getdevices_298',['getDevices',['../group__xv__functions.html#ga9d51997c35d0ab6d4cc2708d6b6b9ab0',1,'xv']]],
  ['getpose_299',['getPose',['../classxv_1_1Slam.html#a67812e6824e6815735ee7552c3b298c0',1,'xv::Slam']]],
  ['getposeat_300',['getPoseAt',['../classxv_1_1Slam.html#a21ee4c479245423ec5ebd74dfb456401',1,'xv::Slam']]]
];
